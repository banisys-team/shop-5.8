<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Log;


class OnlineController extends Controller
{
    public function newProducts()
    {
        $products = Product::where('status', 1)->orderBy('created_at', 'desc')->paginate(10);

        return response()->json($products);
    }

    public function fetchRootCat()
    {
        Log::alert('sssssss');
        $roots = Category::where('parent', null)->with('childrenRecursive')->get();

        return response()->json($roots);
    }

}
