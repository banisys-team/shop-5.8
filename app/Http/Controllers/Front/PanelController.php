<?php

namespace App\Http\Controllers\Front;

use App\Models\Holder;
use App\Models\Size;
use App\Models\User;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Cart_value;
use App\Models\Color;
use App\Models\Effect_price;
use App\Models\Effect_value;
use App\Models\Exist;
use App\Models\Off;
use App\Models\Order;
use App\Models\Order_value;
use App\Models\Product;
use App\Services\MessagingService;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PanelController extends Controller
{
    public function index()
    {
        return view('front.panel.order');
    }

    public function offs()
    {
        return view('front.panel.off');
    }

    public function fetchOrders()
    {
        $user = Auth::user()->id;

        $orders = Order::where('user_id', $user)
            ->where(function ($query) {
                $query->where('reserve', 2)
                    ->orWhereNull('reserve');
            })->orderBy('created_at', 'desc')->paginate(30);

        return response()->json($orders);
    }

    public function account()
    {
        return view('front.panel.account');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => ['required'],
            'postal_code' => ['required'],
            'mobile' => ['required'],
            'state' => ['required'],
            'city' => ['required'],
            'address' => ['required'],
        ];

        $customMessages = [
            'name.required' => 'نام تحویل گیرنده الزامی است',
            'postal_code.required' => 'کد پستی الزامی است',
            'mobile.required' => 'شماره همراه الزامی است',
            'state.required' => 'انتخاب استان الزامی است',
            'city.required' => 'انتخاب شهر الزامی است',
            'address.required' => 'وارد کردن آدرس الزامی است',
        ];

        $this->validate($request, $rules, $customMessages);

        $user = Auth::user();

        $user->name = $request['name'];
        $user->mobile = $request['mobile'];
        $user->postal_code = $request['postal_code'];
        $user->city = $request['city'];
        $user->state = $request['state'];
        $user->address = $request['address'];
        $user->tell = $request['tell'];
        $user->shamsi_c = Verta::instance($user->created_at)->format('Y/n/j');
        $user->shamsi_u = Verta::instance($user->updated_at)->format('Y/n/j');
        $user->save();
        return response()->json(['key' => 'value'], 200);
    }

    public function fetchUser()
    {
        $user = Auth::user();
        if (!file_exists('images/user/profile/' . $user->image)) {
            $user->image = 'review-img.jpg';
            $user->save();
        }
        return response()->json($user);
    }

    public function fetchOffs()
    {
        $user = Auth::user();
        $now = Carbon::now();
        $orders = Order::where('user_id', $user->id)->get();

        if (isset($orders[0]->id)) {
            $sum = 0;
            foreach ($orders as $order) {
                $sum = $sum + $order->sum_final;
            }

            $offs = Off::where('min', '<', $sum)->whereDate('e_date', '>', $now)->get();

            foreach ($offs as $key => $off) {
                foreach ($orders as $order) {
                    if ($off->code == $order->off_code) {
                        unset($offs[$key]);
                    }
                }
            }
        } else {
            $offs = Off::where('min', 0)->whereDate('e_date', '>', $now)->get();
        }

        return response()->json($offs);
    }

    public function storeOrder($Authority, $RefID)
    {
        $order = Order::where('refid', $RefID)->first();
        if (!empty($order)) return false;

        $holder = Holder::where('authority', $Authority)->first();
        $user = User::find($holder->user_id);
        auth()->login($user);

        $order = Order::create([
            'name' => $holder->name,
            'final_total' => $holder->final_total,
            'delivery' => $holder->delivery,
            'state' => $holder->state,
            'city' => $holder->city,
            'address' => $holder->address,
            'description' => $holder->description,
            'reserve' => $holder->reserve,
            'user_id' => $user->id,
            'status' => 0,
            'refid' => $RefID,
        ]);
        $order->update(['shamsi_c' => Verta::instance($order->created_at)->format('Y/n/j')]);

        $carts = Cart::where('cookie', $user->cookie_cart)->get();

        if ($order->reserve == 3) {
            $orders = Order::where('user_id', $user->id)->where('reserve', 1)->orWhere('reserve', 3)->get();
            foreach ($orders as $item) {
                $item->update(['reserve' => 2]);
            }
            $user->update(['wanted' => 1]);
        }

        foreach ($carts as $cart) {
            $product = Product::find($cart->product_id);
            $sizeName = Size::where('id', $cart->size_id)->pluck('name')->first();
            $colorName = Color::where('id', $cart->color_id)->pluck('name')->first();

            $OrderValue = Order_value::create([
                'product_code' => $cart->product_code,
                'product_name' => $product->name,
                'size' => $sizeName,
                'color' => $colorName,
                'number' => $cart->number,
                'discount' => $product->discount,
                'order_id' => $order->id,
                'cart_id' => $cart->id,
                'price' => $product->price,
            ]);
            $exist = Exist::where('product_code', $OrderValue->product_code)->first();
            $exist->update(['num' => $exist->num - $OrderValue->number]);
            if ($exist->num == 0) $exist->delete();

        }

        foreach ($carts as $cart) $cart->delete();

        $user->update([
            'state' => $holder->state,
            'city' => $holder->city,
            'address' => $holder->address,
        ]);

    }
}
