<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Effect_price;
use App\Models\Effect_spec;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class BrandController extends Controller
{
    public function fetchAll()
    {
        $brand = Brand::orderBy('updated_at', 'desc')->get();
        return response()->json($brand);
    }

    public function image(Brand $id)
    {
        $image = $id->image;
        return response()->json($image);
    }

    public function description(Brand $id)
    {
        $description = $id->description;
        return response()->json($description);
    }

    public function fetchBrandCat($cat)
    {
        $cat = Category::find($cat);
        $brands = $cat->brands;

        return response()->json($brands);
    }

    public function editCat(Request $request)
    {
        DB::table('brand_category')->where('brand_id', $request['brand'])->delete();
        $cats = explode(",", $request['cats']);

        foreach ($cats as $cat) {
            DB::table('brand_category')->insert([
                'brand_id' => $request['brand'],
                'cat_id' => $cat,
            ]);
        }

        return response()->json(['key' => 'value'], 200);
    }

    public function fetchRootChild($id)
    {
        $children = Category::where('parent', $id)->with('childrenRecursive')->get();

        return response()->json($children);
    }

    public function fetchRootCat()
    {
        $roots = Category::where('parent', null)->get();

        return response()->json($roots);
    }

    public function store(Request $request)
    {
        $rules = [
            'name_f' => ['max:50', 'unique:brands'],
            'name_e' => ['max:50', 'unique:brands'],
        ];

        $customMessages = [
            'name_f.unique' => 'این نام فارسی قبلا ثبت شده است',
            'name_e.unique' => 'این نام لاتین قبلا ثبت شده است',
            'name_f.max' => 'نام فارسی حداکثر 50 کاراکتر',
            'name_e.max' => ' نام لاتین حداکثر 50 کاراکتر',
        ];

        $this->validate($request, $rules, $customMessages);

        Brand::create([
            'name_f' => $request['name_f'],
            'name_e' => $request['name_e'],
        ]);

        return response()->json(1, 200);
    }

    public function fetch()
    {
        $brands = Brand::orderBy('created_at', 'desc')->paginate(50);

        return response()->json($brands);
    }

    public function searchName(Request $request)
    {

        $result = [];
        if (!empty($request['name_e'])) {
            $result = Brand::where('name_e', 'like', '%' . $request['name_e'] . '%')->orderBy('created_at', 'desc')
                ->paginate(50);
        } elseif (!empty($request['name_f'])) {
            $result = Brand::where('name_f', 'like', '%' . $request['name_f'] . '%')->orderBy('created_at', 'desc')
                ->paginate(50);
            Log::info($result);
        }
        return response()->json($result);
    }

    public function delete($id)
    {
        $product = Product::where('brand_id', $id)->first();
        if (isset($product->id)) {
            return response()->json('cant');
        }

        $brand = Brand::find($id);
        $brand->delete();

        return response()->json(1, 200);
    }

}
