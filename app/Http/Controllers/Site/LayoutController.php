<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Product;

use Illuminate\Http\Request;


class LayoutController extends Controller
{
    public function fetchProByCatId($cat_id)
    {
        $products = Product::where('status', 1)->orderBy('created_at', 'DESC')->paginate(9);

        return response()->json($products);
    }



}




