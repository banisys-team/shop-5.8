<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Suggest;


class SuggestController extends Controller
{
    public function index()
    {
        return view('site.suggest');
    }

    public function fetch()
    {
        $suggests = Suggest::with('product.colors')->orderBy('created_at', 'desc')
            ->paginate(30);

        return response()->json($suggests);
    }


}
