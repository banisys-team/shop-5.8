<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function order_values()
    {
        return $this->hasMany(Order_value::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function track()
    {
        return $this->hasOne(Track::class, 'order_id');
    }
}
