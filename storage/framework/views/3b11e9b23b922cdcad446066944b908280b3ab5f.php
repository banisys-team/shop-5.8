<?php $__env->startSection('content'); ?>
    <div class="container pb-5 pb-lg-0 my-4 my-lg-5 rtl text-right" id="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="mb-4" style="color: #1ac977;font-size: 22px">قوانین استفاده از سایت</h1>
                <p style="text-align: justify;font-size:15px;line-height: 30px;" v-html="rules"></p>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        new Vue({
            el: '#content',
            data: {
                rules: '',
            },
            methods: {
                fetch() {
                    let _this = this;
                    axios.get('/admin/page/fetch').then(res => {
                        _this.rules = res.data.rules
                    })
                },
            },
            mounted() {
                this.fetch()
            }
        });
    </script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>
        body {
            background: white;
        }
    </style>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.site.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/site/page/rules.blade.php ENDPATH**/ ?>