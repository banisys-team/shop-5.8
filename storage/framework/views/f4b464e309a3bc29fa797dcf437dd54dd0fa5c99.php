<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom"
     style="padding-top: 5px;padding-bottom: 0;">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" style="padding: 7px 5px;">
                <img src="/assets/images/menu.svg" style="width: 18px;opacity: .6">
            </a>
        </li>
    </ul>

    <ul class="navbar-nav mr-auto">
        <form method="POST" action="<?php echo e(route('admin.auth.logout')); ?>">
            <?php echo csrf_field(); ?>
            <button style="background-color: unset;border: unset;color: #dc3545;cursor: pointer;padding-left: 0"
                    type="submit">
                <img src="/assets/images/power.svg" style="width:19px;opacity: .8">
            </button>
        </form>
    </ul>
</nav>
<?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/layouts/admin/nav.blade.php ENDPATH**/ ?>