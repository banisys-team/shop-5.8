
<?php $__env->startSection('content'); ?>
<div class="container mt-0 mt-lg-4" id="content">
    <div class="row mb-4 px-2 px-lg-5" v-if="progress">
        <progress :value="percent" max="100" style="width: 100%"></progress>
    </div>
    <div class="row px-lg-3">
        <div class="col-12 p-0">
            <div class="card">
                <div class="card-header"
                     style="padding-bottom: 0;background-color: #343a40;border-bottom: 0px;padding-left: 0;">
                    <ul class="nav nav-pills yy relative d-inline-flex" style="padding-bottom:10px">
                        <li class="nav-item ml-3 mb-2 mb-lg-0">
                            <a class="nav-link active" href="#public"
                               data-toggle="tab">عمومی</a>
                        </li>
                        <li class="nav-item ml-3 mb-2 mb-lg-0">
                            <a class="nav-link" href="#short-desc" data-toggle="tab">
                                توضیحات کوتاه
                            </a>
                        </li>
                        <li class="nav-item ml-3 mb-2 mb-lg-0">
                            <a class="nav-link" href="#gallery" data-toggle="tab">
                                تصاویر</a>
                        </li>
                        <li class="nav-item ml-3 mb-2 mb-lg-0">
                            <a class="nav-link" href="#specification" data-toggle="tab">
                                مشخصات محصول</a>
                        </li>
                        <li class="nav-item ml-3 mb-2 mb-lg-0">
                            <a class="nav-link" href="#coloring" data-toggle="tab">
                                رنگ و سایز </a>
                        </li>
                        <li class="nav-item ml-3 mb-2 mb-lg-0">
                            <a class="nav-link" href="#tbl-size" data-toggle="tab">
                                جدول سایزبندی</a>
                        </li>
                        <li class="nav-item ml-3 mb-2 mb-lg-0 d-lg-none">
                            <button type="button" @click="formValidate" class="submit-btn">
                                ثبت
                            </button>
                        </li>
                    </ul>
                    <button type="button" @click="formValidate"
                            class="submit-btn float-left ml-3 d-none d-lg-block">
                        ثبت
                    </button>
                </div>

                <div class="tab-content p-lg-3">
                    <div class="active tab-pane" id="public">
                        <div class="container pb-3">
                            <div class="row mt-3">
                                <div class="col-md-3 mb-2 mb-lg-0">
                                    <div class="form-group mt-2 mt-lg-0 mb-4 mb-lg-0">
                                        <label>نوع محصول</label>
                                        <br>
                                        <div class="text-right">
                                            <button @click="flag=!flag" id="btn-cat">
                                                {{ form.catName }}
                                                <img id="angle-down" src="/assets/images/arrow-down.svg">
                                            </button>

                                            <div v-if="flag" id="combo-box">

                                                <ul v-if="flag1">
                                                    <li id="root-cat" v-for="root in roots"
                                                        @click="fetchChild(root.id,root.name)">
                                                        {{ root.name }}
                                                        <img class="angle-left"
                                                             src="/assets/images/left-arrow3.svg">
                                                    </li>
                                                </ul>

                                                <ul v-if="flag2">
                                                    <li id="back" @click="back">
                                                        <i class="right">⮞</i>
                                                        <span
                                                            style="margin-right: 6px;padding-top: 3px;display: inline-block;">
                                                                {{ holder.parentName }}
                                                            </span>
                                                    </li>
                                                    <li id="fix" @click="fixCat" v-if="!children.length">
                                                        {{ holder.selfName }}
                                                    </li>
                                                    <li class="children-item" v-for="child in children"
                                                        @click="fetchChild(child.id,child.name,child.children_recursive.length)">
                                                        {{ child.name }}
                                                        <img v-if="child.children_recursive.length"
                                                             class="angle-left"
                                                             src="/assets/images/left-arrow3.svg">
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-lg-none">
                                        <template v-for="(cat,index) in breadcrumbs">
                                            <div class="mb-2 pr-2" style="display: inline-block">
                                                <div class="d-flex flex-row-reverse">
                                                    <i @click="deleteBreadcrumb(index,cat.id)"
                                                       class="remove-breadcrumb">✖</i>
                                                    <span>{{ cat?.name }}</span>
                                                    <img v-if="cat?.parent_recursive[0]?.name"
                                                         src="/assets/images/left-arrow3.svg"
                                                         class="angle-left mx-2 mt-0">
                                                    <span>
                                                            {{ cat?.parent_recursive[0]?.name }}
                                                        </span>
                                                    <img v-if="cat?.parent_recursive[0]?.parent_recursive[0]?.name"
                                                         src="/assets/images/left-arrow3.svg"
                                                         class="angle-left mx-2 mt-0">
                                                    <span>
                                                            {{ cat?.parent_recursive[0]?.parent_recursive[0]?.name }}
                                                    </span>
                                                </div>
                                            </div>
                                            <br>
                                        </template>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3 mb-lg-0">
                                    <div class="form-group mb-4 mb-lg-0">
                                        <label>برند</label>
                                        <button @click="clickBrandBtn" id="btn-cat">
                                            {{ form.brandName }}
                                            <img id="angle-down" src="/assets/images/arrow-down.svg">
                                        </button>
                                        <div v-show="flagBrand" id="sss">
                                            <input type="text" v-model="brandSearch"
                                                   id="input-search"
                                                   @keydown.enter="searchBrand"
                                                   @keyup="checkSearchBrand"
                                                   placeholder="جستجو...">
                                            <ul v-show="flagBrand1">
                                                <li style="line-height: 35px"
                                                    @click="selectBrand('ندارد')">
                                                    <span>ندارد</span>
                                                </li>
                                                <li style="line-height: 35px;"
                                                    @click="selectBrand(brand.name_f)"
                                                    v-for="brand in brands"
                                                    class="all-brand"
                                                    :id="'brand-id'+brand.id">
                                                    <span v-if="brand.name_f">{{ brand.name_f }}</span>
                                                </li>
                                                <li v-show="!brands.length">
                                                    <span style="color: red">موردی یافت نشد</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3 mb-lg-0">
                                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 0">
                                        <label class="col-md-4 control-label">نام محصول</label>
                                        <input type="text" class="form-control"
                                               v-model="form.name">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-lg-5">
                                <div class="col-md-3 mb-3 mb-lg-0">
                                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 0">
                                        <label class="col-md-4 control-label"
                                               style="max-width: 100%;">قیمت</label>
                                        <input type="text" class="form-control"
                                               @keyup="changePrice" v-model="fmt_price">
                                    </div>
                                </div>

                                <div class="col-lg-3 mb-3 mb-lg-0">
                                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 0">
                                        <label class="col-md-4 control-label" style="max-width: 100%;">قیمت
                                            پس
                                            از تخفیف
                                        </label>
                                        <input @keyup="calDiscount" type="text" class="form-control"
                                               v-model="fmt_afterDiscount">
                                    </div>
                                </div>

                                <div class="col-lg-3 mb-3 mb-lg-0">
                                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 4px">
                                        <label class="col-md-4 control-label">تخفیف</label>
                                        <input @keyup="calDiscountPrice" type="text" class="form-control"
                                               v-model="form.discount">
                                    </div>

                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group" style="margin-bottom: 0">
                                        <label for="exampleFormControlSelect3">وضعیت</label>
                                        <select class="form-control" id="exampleFormControlSelect3"
                                                v-model="form.status">
                                            <option value="" disabled hidden>انتخاب کنید...</option>
                                            <option value="1">نمایش در سایت</option>
                                            <option value="0">عدم نمایش در سایت</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row mt-lg-4">
                                <div class="col-lg-7 mt-4 mt-lg-2 pr-3 d-none d-lg-block">
                                    <template v-for="(cat,index) in breadcrumbs">
                                        <div class="mb-2" style="display: inline-block">
                                            <div class="d-flex flex-row-reverse">
                                                <i @click="deleteBreadcrumb(index,cat.id)"
                                                   class="remove-breadcrumb">✖</i>
                                                <span>{{ cat?.name }}</span>
                                                <img v-if="cat?.parent_recursive[0]?.name"
                                                     src="/assets/images/left-arrow3.svg"
                                                     class="angle-left mx-2 mt-0">
                                                <span>
                                                            {{ cat?.parent_recursive[0]?.name }}
                                                        </span>
                                                <img v-if="cat?.parent_recursive[0]?.parent_recursive[0]?.name"
                                                     src="/assets/images/left-arrow3.svg"
                                                     class="angle-left mx-2 mt-0">
                                                <span>
                                                            {{ cat?.parent_recursive[0]?.parent_recursive[0]?.name }}
                                                    </span>
                                            </div>
                                        </div>
                                        <br>
                                    </template>
                                </div>
                                <div class="col-lg-3 mt-4">
                                    <img style="width:100%;border-radius: 8px" id="blah" class="px-4 px-lg-0"
                                         v-show="!flagThumbnail"
                                         :src="'/images/product/'+form.image"/>
                                    <img v-show="flagThumbnail" id="thumbnail" class="px-4 px-lg-0"
                                         style="width: 100%;border-radius: 8px">
                                </div>
                                <div class="col-lg-2 mt-4 text-left">
                                    <input @change="thumbnailChange"
                                           type="file" name="file" id="input" class="inputfile">
                                    <label style="font-weight: unset" class="thumbnail-img" for="input">تصویر
                                        شاخص</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="short-desc">
                        <div class="row mb-4">
                            <div class="col-12 p-0">
                                <div class="form-group">
                                    <textarea type="text" name="short_desc"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane" id="gallery">
                        <div class="row mt-3 pb-3 px-2">
                            <div class="container">
                                <div class="row mt-3 pb-4 px-2" style="border-bottom: 2px dashed #a3a3a3;">
                                    <div class="col-12 text-center text-lg-right">
                                        <input @change="galleryChange"
                                               type="file" name="file" id="input2" class="inputfile">
                                        <label class="btn btn-primary" for="input2"
                                               style="font-weight: unset;border-bottom: 3px solid #2269a3;">
                                            + افزودن تصویر گالری
                                        </label>
                                    </div>

                                    <div v-show="galleries.length" class="col-5 col-lg-2 mt-4 p-0 mx-2"
                                         v-for="(image, index) in galleries"
                                         :key="index" style="position: relative">
                                        <a @click="deleteImage2(index)">
                                            <i class="fa fa-times del">✖</i>
                                        </a>
                                        <img :src="image" style="width: 100%;border-radius: 8px">
                                    </div>


                                </div>
                                <div class="row mt-lg-4 px-lg-4 mx-2">
                                    <div class="col-6 col-lg-3 mt-4 mb-4" v-for="img in gallery">
                                        <img style="position: relative;width:100%;border-radius: 8px"
                                             class="img-fluid" :src="'/images/gallery/'+img.image">
                                        <a @click="deleteGallery(img.id)">
                                            <i class="del2">✖</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="specification">
                        <div class="row mt-3">
                            <div class="col-12">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr v-for="specification in specifications2">
                                        <td>{{specification.key}} :</td>
                                        <td>{{specification.value}}</td>
                                        <td>
                                            <a @click="deleteSpecification(specification.id)"
                                               style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                                ✖
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 text-center text-lg-right">
                                <div class="form-group">

                                    <button class="btn btn-info mb-4"
                                            @click.stop.prevent="addRowSpecification()">
                                        <i style="font-weight: bold">+</i> افزودن مشخصه
                                    </button>

                                    <ul class="container-fluid" style="list-style:none;">
                                        <li class="row" v-for="(specification, index) in specifications">
                                            <input type="text" v-model="specification.key"
                                                   placeholder="عنوان"
                                                   class="form-control col-5 col-lg-3 my-2 ml-lg-2"
                                                   style="display: inline-block">
                                            <input type="text" v-model="specification.value"
                                                   placeholder="مقدار"
                                                   class="form-control col-6 col-lg-5 my-2 ml-lg-2"
                                                   style="display: inline-block">
                                            <a class="col-1 col-lg-3 p-0" @click="deleteRowSpecification(index)"
                                               style="font-size: 20px;">
                                                <i class="mr-lg-2 d-inline-block pt-3"
                                                   style="color: #dc3545;font-weight: bold;font-size: 16px;">✖</i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="coloring">
                        <div class="row mt-3 px-3">
                            <div class="col-lg-6">
                                <div class="row mt-3 mb-5" v-if="colors2.length">
                                    <div class="col-12">
                                        <table class="table table-striped">
                                            <tbody>
                                            <tr v-for="color in colors2">
                                                <td>{{color.name}}</td>
                                                <td>
                                                            <span :style="{backgroundColor: color.code}"
                                                                  style="width: 30px;height:30px;display: block;border-radius:800px;border: solid 1px grey">
                                                    </span>
                                                </td>
                                                <td>
                                                    <a @click="deleteColor(color.id)"
                                                       style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                                        ✖
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <button class="btn btn-info mb-3" @click.stop.prevent="addRow()">
                                                <i style="font-weight: bold">+</i> افزودن رنگ
                                            </button>
                                            <ul style="list-style:none;" class="container-fluid">
                                                <li class="row" v-for="(color, index) in colors">
                                                    <input type="text" v-model="color.name"
                                                           placeholder="نام رنگ"
                                                           class="form-control col-8 col-lg-4 my-2 ml-2"
                                                           style="display: inline-block">
                                                    <input type="color" v-model="color.code"
                                                           class="form-control col-2 col-lg-1 ml-2 mt-2"
                                                           style="height: 39px;display: inline-block;width: 40px;padding: 0;vertical-align: middle;">
                                                    <a class="col-1" @click="deleteRow(index)"
                                                       style="font-size: 20px;">
                                                        <i class="mr-2 d-inline-block pt-3"
                                                           style="color: #dc3545;font-weight: bold;font-size: 16px;">✖</i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row mt-3 mb-5" v-if="sizes2.length">
                                    <div class="col-12">
                                        <table class="table table-striped">
                                            <tbody>
                                            <tr v-for="size in sizes2">
                                                <td>{{size.name}}</td>
                                                <td>
                                                    <a @click="deleteSize(size.id)"
                                                       style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                                        ✖
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <button class="btn btn-info mb-3"
                                                    @click.stop.prevent="addRowSize()">
                                                <i style="font-weight: bold">+</i> افزودن سایز
                                            </button>
                                            <ul style="list-style:none;">
                                                <li v-for="(size, index) in sizes">
                                                    <input type="text" v-model="size.name"
                                                           placeholder="عنوان"
                                                           class="form-control col-10 col-lg-4 my-2 ml-2"
                                                           style="display: inline-block">

                                                    <a @click="deleteRowSize(index)" style="font-size: 20px;">
                                                        <i class="mr-2"
                                                           style="color: #dc3545;font-weight: bold;font-size: 16px;">✖</i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tbl-size">
                        <div class="row mt-0 mt-lg-3">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea type="text" name="tbl_size"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="img-container" style="height: 80vh">
                        <img id="image" src="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">انصراف</button>
                    <button type="button" class="btn btn-primary" id="crop" @click="cropThumbnail">برش</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="img-container" style="height: 80vh">
                        <img id="image2" src="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">انصراف</button>
                    <button type="button" class="btn btn-primary" id="crop" @click="cropGallery">برش</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('js/ckeditor/ckeditor.js')); ?>"></script>
<script src="/js/cropper.min.js"></script>
<script>
    let vm = new Vue({
        el: '#content',
        data: {
            form: {
                name: null,
                discount: null,
                price: null,
                roles: null,
                image: null,
                brandName: null,
                catName: 'انتخاب کنید...',
                status: null,
                fmt_afterDiscount: null,
                afterDiscount: null,
                cats: [],
                galleries: []
            },
            cats: [],
            specifications: [],
            specifications2: [],
            colors: [],
            product_id: '',
            brands: [],
            attachments: [],
            counter: [],
            isDragging: false,
            dragCount: 0,
            files: [],
            images: [],
            product: '',
            gallery: [],
            effect_prices: '',
            effect_val: {},
            effect_values: [],
            color_images: [],
            color_images_demo: [],
            colors2: [],
            pics: [],
            showModal: false,
            colorCounter: 0,
            progress: false,
            percent: 0,
            // tree
            categories: [],
            output: '',
            flag: false,
            flag1: true,
            flag2: false,
            roots: [],
            children: [],
            flagBrand: false,
            brandSearch: '',
            flagBrand1: true,
            holder: {
                selfName: 'ریشه',
                selfId: '',
                parentName: '',
                parentId: '',
                grandName: '',
                grandId: '',
            },
            sizes: [],
            sizes2: [],
            cropper: null,
            cropper2: null,
            galleries: [],
            formData: null,
            flagThumbnail: false,
            breadcrumbs: [],
        },
        computed: {
            fmt_price: {
                get() {
                    if (this.form.price) {

                        this.form.price = this.fixNumbers(this.form.price)

                        return this.commafy(this.form.price)
                    } else
                        return ''
                },
                set(newValue) {
                    this.form.price = this.fixNumbers(this.form.price)
                    this.form.price = parseFloat(newValue.replace(/,/g, ""));
                }
            },
            fmt_afterDiscount: {
                // getter
                get: function () {
                    if (this.form.afterDiscount) {
                        return this.commafy(this.form.afterDiscount)
                    } else {
                        // this.form.discount = ""
                        return ""
                    }
                },
                set: function (newValue) {
                    this.form.afterDiscount = parseFloat(newValue.replace(/,/g, ""))
                }
            },
        },
        methods: {
            fixNumbers(str) {
                var persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g]
                var arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g]
                if (typeof str === 'string') {
                    for (var i = 0; i < 10; i++) {
                        str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i)
                    }
                }
                return str
            },
            deleteBreadcrumb(index, id) {
                this.breadcrumbs.splice(index, 1)
                this.form.cats = this.form.cats.filter(item => (item !== id))
            },
            galleryChange(e) {
                var image = document.getElementById('image2')
                var input = document.getElementById('input2')
                var files = e.target.files
                var $modal = $('#modal2')
                var done = function (url) {
                    input.value = ''
                    image.src = url
                    $modal.modal('show')
                }

                if (files && files.length > 0) {
                    done(URL.createObjectURL(files[0]));
                }
            },
            thumbnailChange(e) {
                var image = document.getElementById('image')
                var input = document.getElementById('input')
                var files = e.target.files
                var $modal = $('#modal')
                var done = function (url) {
                    input.value = ''
                    image.src = url
                    $modal.modal('show')
                }

                if (files && files.length > 0) {
                    done(URL.createObjectURL(files[0]));
                }
            },
            getCanvasBlob(canvas) {
                return new Promise(function (resolve, reject) {
                    canvas.toBlob(function (blob) {
                        resolve(blob)
                    })
                })
            },
            async cropGallery() {
                var $modal = $('#modal2')
                var canvas
                $modal.modal('hide')

                canvas = this.cropper2.getCroppedCanvas({
                    width: 600,
                    height: 700,
                })

                let finalImg = await this.getCanvasBlob(canvas)
                this.galleries.push(canvas.toDataURL())
                this.form.galleries.push(finalImg)

            },
            async cropThumbnail() {
                vm.flagThumbnail = true
                var $modal = $('#modal')
                var thumbnail = document.getElementById('thumbnail');
                var canvas
                $modal.modal('hide')

                canvas = this.cropper.getCroppedCanvas({
                    width: 600,
                    height: 700,
                })

                thumbnail.src = canvas.toDataURL()
                let finalImg = await this.getCanvasBlob(canvas)
                this.formData.append('image', finalImg)

            },
            commafy(num, prec, currSign) {
                if (prec == null) prec = 0;
                var str = parseFloat(num).toFixed(prec).toString().split('.');
                if (str[0].length >= 4) {
                    str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
                }
                if (str[1] && str[1].length >= 4) {
                    str[1] = str[1].replace(/(\d{3})/g, '$1 ');
                }
                return (currSign == null ? "" : currSign + " ") + str.join('.');
            },
            searchBrand() {
                let vm = this
                if (this.brandSearch === '') {

                } else {
                    axios.get(`/admin/product/search/brand/${this.brandSearch}`).then(res => {
                        vm.brands = res.data
                    })
                }
            },
            checkSearchBrand() {
                let vm = this
                if (this.brandSearch === '') {
                    this.fetchBrands()
                }
            },
            selectBrand(name) {
                this.flagBrand = false
                this.form.brandName = name
            },
            clickBrandBtn() {
                if (this.flagBrand === false) {
                    this.flagBrand = true;
                } else if (this.flagBrand === true) {
                    this.flagBrand = false
                }
            },
            calDiscount() {
                if (!this.form.price) {
                    swal.fire(
                        {
                            text: 'لطفا ابتدا قیمت را وارد کنید .',
                            type: 'warning',
                            confirmButtonText: 'باشه',
                        }
                    )
                    this.form.afterDiscount = ''
                } else {
                    if (this.form.afterDiscount) {

                        let per = this.form.price / 100
                        let x = this.form.afterDiscount / per
                        this.form.discount = 100 - x
                    } else {
                        this.form.discount = 0
                    }
                }
            },
            calDiscountPrice() {
                if (this.form.price === null) {
                    swal.fire(
                        {
                            text: 'لطفا ابتدا قیمت را وارد کنید .',
                            type: 'warning',
                            confirmButtonText: 'باشه',
                        }
                    )
                    this.form.discount = 0
                } else {
                    let per = this.form.price / 100
                    let x = 100 - this.form.discount

                    this.form.afterDiscount = x * per
                }
            },
            changePrice() {
                this.form.afterDiscount = null
                this.form.discount = 0
            },
            OnDragEnter(e) {
                e.preventDefault();
                this.dragCount++;
                this.isDragging = true;
                return false;
            },
            OnDragLeave(e) {
                e.preventDefault();
                this.dragCount--;
                if (this.dragCount <= 0)
                    this.isDragging = false;
            },
            onInputChange(e) {
                const files = e.target.files;
                Array.from(files).forEach(file => this.addImage(file));
            },
            onDrop(e) {
                e.preventDefault();
                e.stopPropagation();
                this.isDragging = false;
                const files = e.dataTransfer.files;
                Array.from(files).forEach(file => this.addImage(file));
            },
            addImage(file) {
                if (!file.type.match('image.*')) {
                    this.$toastr.e(`${file.name} is not an image`);
                    return;
                }
                this.files.push(file);
                const reader = new FileReader();
                reader.onload = (e) => this.images.push(e.target.result);
                reader.readAsDataURL(file);
            },
            deleteImage(index) {
                this.images.splice(index, 1);
                this.files.splice(index, 1);
            },
            deleteImage2(index) {
                this.galleries.splice(index, 1)
                this.form.galleries.splice(index, 1)
            },
            getFileSize(size) {
                const fSExt = ['Bytes', 'KB', 'MB', 'GB'];
                let i = 0;

                while (size > 900) {
                    size /= 1024;
                    i++;
                }
                return `${(Math.round(size * 100) / 100)} ${fSExt[i]}`;
            },
            addRow() {
                this.colors.push({
                    name: '',
                    code: ''
                });
            },
            addRowSpecification() {
                this.specifications.push({})
            },
            addRowSize() {
                this.sizes.push({})
            },
            deleteRowSpecification(index) {
                this.specifications.splice(index, 1)
            },
            deleteRowSize(index) {
                this.sizes.splice(index, 1)
            },
            deleteRow(index) {
                this.colors.splice(index, 1)
            },
            onImageChange(e) {
                this.form.image = e.target.files[0];
            },
            fieldChange(e) {
                let selectedFiles = e.target.files;
                if (!selectedFiles.length) {
                    return false;
                }
                for (let i = 0; i < selectedFiles.length; i++) {
                    this.attachments.push(selectedFiles[i]);
                }
            },
            fetchCat() {
                let data = this;
                axios.get('/admin/catspec/cat').then(res => {
                    data.cats = res.data;

                });
            },
            fetchBrand() {
                let data = this;
                axios.get(`/admin/product/fetch/brand/all`).then(res => {
                    data.brands = res.data
                });
            },
            async fetchProduct() {
                let data = this;
                await axios.get(`/admin/product/fetch/${window.id}`).then(res => {
                    data.product = res.data
                    data.form.name = data.product.name
                    data.form.price = data.product.price
                    data.form.discount = data.product.discount
                    data.product.cats.forEach(cat => {
                        data.form.cats.push(cat.id);
                    })

                    if (data.product.brand) {
                        data.form.brandName = data.product.brand.name_f
                    } else {
                        data.form.brandName = 'ندارد'
                    }

                    data.form.status = data.product.status
                    data.form.image = data.product.image
                    CKEDITOR.instances["short_desc"].setData(data.product.short_desc)
                    CKEDITOR.instances["tbl_size"].setData(data.product.tbl_size)
                })
                this.calDiscountPrice()
            },
            getEffectValues(id) {
                data = this;
                axios.get(`/admin/product/fetch/effect/values/${id}`).then(res => {
                    data.effect_values = res.data;
                });
            },
            getColors() {
                let vm = this
                axios.get(`/admin/product/colors/fetch/${window.id}`).then(res => {
                    vm.colors2 = res.data
                })
            },
            getGallery() {
                let vm = this
                axios.get(`/admin/product/fetch/gallery/${window.id}`).then(res => {
                    vm.gallery = res.data
                })
            },
            getValue(key) {
                result = this.values.filter(value => value.key === key);
                if (result.length) {
                    return result[0].value;
                }
            },
            getEffectVal(key) {
                result = this.effect_values.filter(value => value.key === key);
                if (result.length) {
                    return result[0].value;
                } else {
                    return 0;
                }
            },
            deleteGallery(id) {
                swal.fire({
                    text: "آیا از پاک کردن اطمینان دارید ؟",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'بله',
                    cancelButtonText: 'لغو',
                }).then((result) => {
                    if (result.value) {
                        axios.get(`/admin/product/delete/gallery/${id}`).then(res => {
                            parts = window.location.href.split('/');
                            pro = parts.pop() || parts.pop();
                            this.getGallery(pro);
                        });
                    }
                });
            },
            fetchEffectPrices(id) {
                let data = this;
                axios.get(`/admin/product/effect/price/${id}`).then(res => {
                    data.effect_prices = res.data;

                });
            },
            onInputChange2(e) {
                const files = e.target.files;
                let x = [];
                Array.from(files).forEach(file => x.push(file));
                this.color_images[this.colorCounter] = x;
                this.colorCounter++;
            },
            addImage2(file) {
                this.color_images.push(file);
            },
            showImage(id) {
                let data = this;
                this.showModal = true;
                this.pic = true;
                axios.get(`/admin/product/color/image/${id}`).then(function (res) {
                    data.pics = res.data;
                });
            },
            formSubmit() {
                let vm = this
                this.progress = true
                let config = {
                    headers: {'content-type': 'multipart/form-data'},
                    onUploadProgress: (e) => {
                        this.percent = Math.round((e.loaded / e.total) * 100)
                    }
                }

                let colors = JSON.stringify(this.colors)
                let specifications = JSON.stringify(this.specifications)
                let sizes = JSON.stringify(this.sizes)

                this.form.galleries.forEach(gallery => {
                    this.formData.append('pics[]', gallery)
                })

                this.form.cats.forEach(cat => {
                    this.formData.append('cats[]', cat)
                })

                let shortDesc = CKEDITOR.instances["short_desc"].getData()
                let tblSize = CKEDITOR.instances["tbl_size"].getData()

                this.formData.append('image', this.form.image)
                this.formData.append('name', this.form.name)
                this.formData.append('brand_name', this.form.brandName)
                this.formData.append('price', this.form.price)
                this.formData.append('status', this.form.status)
                this.formData.append('discount', this.form.discount)
                this.formData.append('colors', colors)
                this.formData.append('specifications', specifications)
                this.formData.append('short_desc', shortDesc)
                this.formData.append('tbl_size', tblSize)
                this.formData.append('sizes', sizes)

                axios.post(`/admin/product/update/${id}`, this.formData, config).then((res) => {
                    swal.fire({
                        text: "تغییرات با موفقیت اعمال شد !",
                        icon: "success",
                        confirmButtonText: 'باشه',
                    })
                    vm.progress = false
                    vm.fetchProduct()
                    vm.getColors()
                    vm.getGallery()
                    vm.fetchSpecification()
                    vm.fetchSizes()
                    vm.images = []
                    vm.files = []
                    vm.colors = []
                    vm.specifications = []
                    vm.sizes = []
                    vm.galleries = []
                    vm.form.galleries = []
                    vm.gallery = []
                    vm.form.cats = []
                    vm.formData.delete('pics[]')
                    vm.formData.delete('cats[]')
                })
            },
            formValidate() {
                if (!this.form.cats.length) {
                    swal.fire({
                        type: 'warning',
                        text: 'نوع محصول را انتخاب کنید.',
                        confirmButtonText: 'باشه',
                    })
                    return
                }
                if (this.form.name === '') {
                    swal.fire(
                        {
                            text: 'نام محصول را وارد کنید',
                            icon: 'warning',
                            confirmButtonText: 'باشه',
                        }
                    )
                    return
                }
                if (this.form.price === null) {
                    swal.fire(
                        {
                            text: 'قیمت را وارد کنید',
                            icon: 'warning',
                            confirmButtonText: 'باشه',
                        }
                    )
                    return
                }
                this.formSubmit()
            },
            // tree
            clickCatBtn() {
                if (this.flag === false) {
                    this.flag = true;
                } else if (this.flag === true) {
                    this.flag = false
                }
            },
            fetchRootCat() {
                let data = this;
                axios.get(`/admin/product/fetch/cat/root`).then(res => {
                    data.roots = res.data
                });
                this.holder.parentName = 'ریشه'
                this.holder.parentId = ''
                this.holder.grandName = 'ریشه'
                this.holder.grandId = ''
            },
            back() {
                if (this.holder.parentName === 'ریشه') {
                    this.flag1 = true
                    this.flag2 = false
                    this.holder.selfName = 'ریشه'
                    this.holder.selfId = ''
                    this.holder.parentName = ''
                    this.holder.parentId = ''
                    this.holder.grandName = ''
                    this.holder.grandId = ''
                } else {
                    this.children = this.categories.filter(cat => cat.parent === this.holder.parentId)

                    this.holder.selfName = this.holder.parentName
                    this.holder.selfId = this.holder.parentId

                    this.holder.parentName = this.holder.grandName
                    this.holder.parentId = this.holder.grandId

                    let x = this.categories.filter(cat => cat.id === this.holder.grandId)
                    if (x[0]) {
                        let y = this.categories.filter(cat => cat.id === x[0].parent)
                        if (y[0]) {
                            this.holder.grandName = y[0].name
                            this.holder.grandId = y[0].id
                        } else {
                            this.holder.grandName = 'ریشه'
                            this.holder.grandId = ''
                        }
                    }
                }
            },
            fetchChild(id, name, length) {
                this.flag1 = false
                this.flag2 = true

                this.holder.grandName = this.holder.parentName
                this.holder.grandId = this.holder.parentId

                this.holder.parentName = this.holder.selfName
                this.holder.parentId = this.holder.selfId

                this.holder.selfName = name
                this.holder.selfId = id

                if (length === 0) {
                    this.fixCat()
                    this.holder.selfName = 'ریشه'
                    // this.holder.selfId = ''
                    this.holder.parentName = ''
                    this.holder.parentId = ''
                    this.holder.grandName = ''
                    this.holder.grandId = ''
                    this.flag1 = true
                    this.flag2 = false
                    this.fetchCategories()
                    return
                }

                this.children = this.categories.filter(cat => cat.parent === id)
            },
            fixCat() {
                // this.form.catName = this.holder.selfName
                this.form.cats.push(this.holder.selfId)
                this.flag = false
                this.fetchBreadcrumb()

            },
            fixRoot() {
                this.form.catName = 'ریشه';
                this.flag = false;
            },
            fetchSpecification() {
                let vm = this
                axios.get(`/admin/product/specifications/fetch/${window.id}`).then(res => {
                    vm.specifications2 = res.data
                })
            },
            deleteSpecification(id) {
                axios.get(`/admin/product/specifications/delete/${id}`).then(res => {
                    swal.fire(
                        {
                            text: "با موفقیت حذف شد!",
                            type: "success",
                            confirmButtonText: 'باشه',
                        }
                    )
                })
                this.fetchSpecification()
            },
            deleteColor(id) {
                axios.get(`/admin/product/color/delete/${id}`).then(res => {
                    swal.fire(
                        {
                            text: "با موفقیت حذف شد!",
                            type: "success",
                            confirmButtonText: 'باشه',
                        }
                    )
                })
                this.getColors()
            },
            deleteSize(id) {
                axios.get(`/admin/product/size/delete/${id}`).then(res => {
                    swal.fire(
                        {
                            text: "با موفقیت حذف شد!",
                            type: "success",
                            confirmButtonText: 'باشه',
                        }
                    )
                })
                this.fetchSizes()
            },
            fetchSizes() {
                let vm = this
                axios.get(`/admin/product/sizes/fetch/${window.id}`).then(res => {
                    vm.sizes2 = res.data
                })
            },
            fetchCategories() {
                let _this = this
                axios.get(`/admin/category/fetch`).then(res => {
                    _this.categories = res.data
                    _this.roots = res.data.filter(cat => cat.parent === null)
                })
            },
            fetchBreadcrumb() {
                axios.post('/admin/product/fetch/breadcrumb', {
                    catIds: this.form.cats
                }).then(res => {
                    vm.breadcrumbs = res.data
                })
            },
        },
        async mounted() {
            let parts = window.location.href.split('/')
            window.id = parts.pop() || parts.pop()
            this.fetchCategories()
            await this.fetchBrand()
            await this.fetchProduct()
            await this.getColors()
            await this.getGallery()
            await this.fetchSpecification()
            await this.fetchSizes()
            await this.fetchBreadcrumb()

            this.formData = new FormData
            var $modal = $('#modal')
            var image = document.getElementById('image')
            $modal.on('shown.bs.modal', function () {
                vm.cropper = new Cropper(image, {
                    viewMode: 2,
                    aspectRatio: 600 / 700,
                    data: {
                        width: 600,
                        height: 700,
                    },
                })
            }).on('hidden.bs.modal', function () {
                vm.cropper.destroy()
                vm.cropper = null
            })


            var $modal2 = $('#modal2')
            var image2 = document.getElementById('image2');
            $modal2.on('shown.bs.modal', function () {
                vm.cropper2 = new Cropper(image2, {
                    viewMode: 2,
                    aspectRatio: 600 / 700,
                    data: {
                        width: 600,
                        height: 700,
                    },
                })
            }).on('hidden.bs.modal', function () {
                vm.cropper2.destroy()
                vm.cropper2 = null
            })
        }
    })
</script>
<script src="<?php echo e(asset('js/ckeditor/ckeditor.js')); ?>"></script>
<script>
    CKEDITOR.replace('tbl_size', {
        customConfig: "<?php echo e(asset('/js/ckeditor/config.js')); ?>",
        height: 500
    })
    CKEDITOR.replace('short_desc', {
        customConfig: "<?php echo e(asset('/js/ckeditor/config.js')); ?>",
        height: 500,
    })
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader()
            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(300)
            }
            reader.readAsDataURL(input.files[0])
        }
    }
</script>
<script>
    $("#side_product").addClass("menu-open");
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<link rel="stylesheet" href="/css/cropper.min.css">
<style>
    .btn-info {
        border-bottom: 3px solid #0c7384;
    }

    i {
        font-style: normal !important;
    }

    .thumbnail-img {
        font-size: 15px;
        font-weight: lighter !important;
        padding: 5px 18px;
        color: #ffffff;
        border-radius: 5px;
        vertical-align: top;
        cursor: pointer;
        background: #dc3545;
        border-bottom: 3px solid #ac1f2d;
    }

    .thumbnail-hint {
        position: absolute;
        top: 40px;
        left: 20px;
        color: #808080;
        font-size: 14px;
    }

    #btn-cat {
        position: relative;
        background-color: white;
        border: 1px solid #ced4da;
        width: 100%;
        padding: 8px 8px 4px 8px;
        border-radius: 5px;
        text-align: right;
        color: #484848;
    }

    #btn-cat:focus {
        outline: unset;
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        box-shadow: inset 0 0 0 transparent, 0 0 0 0.2rem rgb(0 123 255 / 25%);
    }

    .yy .active {
        border-bottom-left-radius: 0 !important;
        border-bottom-right-radius: 0 !important;
        border-top-left-radius: 10px !important;
        border-top-right-radius: 10px !important;
        color: #101010 !important;
        background-color: #ffffff !important;
        font-weight: bold;
    }

    .nav-pills .nav-link {
        color: #c2c7d0;
    }

    #cke_editor1, #cke_editor2 {
        width: 99%
    }

</style>
<style>
    .del {
        color: rgb(220, 53, 69);
        position: absolute !important;
        left: 3px !important;
        bottom: 4px !important;
        font-size: 23px !important;
    }

    .del2 {
        color: rgb(220, 53, 69);
        position: absolute !important;
        left: 18px !important;
        top: 6px !important;
        font-size: 23px !important;
    }

    .uploader {
        width: 100%;
        background: #ffffff;
        color: #a8a8a8;
        padding: 15px;
        text-align: center;
        border-radius: 15px;
        border: 3px dashed #a8a8a8;
        font-size: 20px;
        position: relative;
    }

    .uploader.dragging {
        background: #fff;
        color: #cecece;
        border: 3px dashed #cecece;
    }

    .uploader.dragging .file-input label {
        background: #a8a8a8;
        color: #545454;
    }

    .uploader i {
        font-size: 85px;
    }

    .uploader .file-input {
        width: 200px;
        margin: auto;
        height: 68px;
        position: relative;
    }

    .uploader .file-input label, .uploader .file-input input {
        background: #a8a8a8;
        color: #ffffff;
        width: 100%;
        position: absolute;
        left: 0;
        top: 0;
        padding: 10px;
        border-radius: 4px;
        margin-top: 7px;
        cursor: pointer;
    }

    .uploader .file-input input {
        opacity: 0;
        z-index: -2;
    }

    .uploader .images-preview {
        display: flex;
        flex-wrap: wrap;
        margin-top: 30px;
    }

    .uploader .images-preview .img-wrapper {
        width: 160px;
        display: flex;
        flex-direction: column;
        margin: 10px;
        justify-content: space-between;
        background: #fff;
        box-shadow: -8px 8px 6px #a0a0a0;
        overflow: hidden;
    }

    .uploader .images-preview .details {
        font-size: 12px;
        background: #fff;
        color: #000;
        display: flex;
        flex-direction: column;
        align-items: self-start;
        padding: 3px 6px;
    }

    .uploader .images-preview .details .name {
        overflow: hidden;
        height: 18px;
    }

    .uploader .upload-control {
        position: absolute;
        width: 100%;
        background: #fff;
        top: 0;
        left: 0;
        border-top-left-radius: 7px;
        border-top-right-radius: 7px;
        padding: 10px;
        padding-bottom: 4px;
        text-align: right;
    }

    .uploader .upload-control button, .uploader .upload-control label {
        background: #cecece;
        border: 2px solid #cecece;
        border-radius: 3px;
        color: #fff;
        font-size: 15px;
        cursor: pointer;
    }

    .uploader .upload-control label {
        padding: 2px 5px;
        margin-right: 10px;
    }

    .nav-pills .nav-link:not(.active):hover {
        color: #ffffff !important;
    }
</style>
<style>
    .modal-mask {
        position: fixed !important;
        z-index: 9998 !important;
        top: 0 !important;
        left: 0 !important;
        height: 100vh !important;
        background-color: rgba(0, 0, 0, .5) !important;
        display: table !important;
        transition: opacity .3s ease !important;
    }

    .modal-content {
        max-height: calc(100vh - -3.5rem) !important;
        height: 100vh
    }

    .submit-btn {
        background: rgb(9, 144, 32);
        border: unset;
        color: white;
        width: 70px;
        height: 31px;
        border-radius: 50px;
        cursor: pointer;
        font-size: 15px;
    }

    .nav-pills .nav-link.active {
        background-color: #ffffff !important;
        color: #4e4e4e !important;
        font-weight: bold;
    }

    .nav-item .active {
        border-radius: 50px !important;
    }

    .nav-item {
        font-size: 15px;
    }

    .nav-link {
        padding: 5px 10px !important;
    }

    #sss {
        position: absolute;
        list-style: none;
        top: 70px;
        z-index: 99;
        background-color: white;
        padding: 5px 15px;
        width: 100%;
        line-height: 35px;
        border: 1px solid #a1a1a1;
        max-height: 330px;
        overflow: scroll;
        overflow-x: hidden;
    }

    #sss li {
        cursor: pointer
    }

    .left {
        float: right;
        font-weight: bold;
        font-size: 22px;
        margin: 3px -2px 0 5px;
    }

    li {
        list-style: none
    }

    #input-search {
        display: block;
        border-bottom: 1px solid #afafaf;
        border-left: unset;
        border-right: unset;
        border-top: unset;
        width: 100%;
        margin-bottom: 10px;
    }

    .brand-select {
        line-height: 35px;
        padding: 0px 10px;
        border-radius: 5px;
        background-color: #358fdc;
        color: white;
        margin-top: 2px;
        margin-bottom: 2px;
        transition: all 0.3s;
    }

    .form-group label {
        font-size: 15px;
    }

    i {
        font-style: unset;
    }

</style>
<style>
    .inputfile {
        /* visibility: hidden etc. wont work */
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile:focus + label {
        /* keyboard navigation */
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .inputfile + label * {
        pointer-events: none;
    }

    #modal2 {
        z-index: 99999999;
    }

    .modal-dialog {
        width: 100%;
        height: 100vh !important;
        margin: 0;
        padding: 0;
        max-width: 100% !important;
    }

    .modal-content {
        width: 100%;
        height: 100vh !important;
        min-height: 100%;
        border-radius: 0;
    }

    .modal-body {
        height: 100vh !important;
    }

    #modal {
        z-index: 99999999;
    }

    @media (min-width: 576px) {
        .container {
            max-width: unset !important;
        }
    }

    #cke_1_top {
        padding: 4px 0
    }

    <
    style >
    .children-item {
        margin-right: 10px;
    }

    #fix {
        color: black;
        font-weight: bold;
        background-color: #f1f1f1;
        padding-right: 6px;
        border-radius: 6px
    }

    .right {
        float: right;
        margin: 11px 0 0 5px;
    }

    #back {
        color: #a0a0a0
    }

    #root-cat {
        line-height: 35px;
    }

    #root {
        color: black;
        font-weight: bold;
        background-color: #f1f1f1;
        padding-right: 6px;
        border-radius: 6px
    }

    li {
        list-style: none
    }

    #btn-cat {
        position: relative;
        background-color: white;
        border: 1px solid #ced4da;
        width: 100%;
        padding: 8px 8px 0 8px;
        border-radius: 5px;
        text-align: right;
        color: #484848;
        height: 41px;
        overflow: hidden;
    }

    #angle-down {
        float: left;
        width: 11px;
        margin: 5px 2px 0px 5px;
    }

    #combo-box {
        position: absolute;
        list-style: none;
        z-index: 99;
        background-color: white;
        padding: 5px 10px;
        width: 97%;
        line-height: 35px;
        border: 1px solid #c8c8c8;
        overflow-x: hidden;
        max-height: 350px;
        border-radius: 0 0 8px 8px;
    }

    .angle-left {
        float: left;
        margin-top: 14px;
        color: #636363;
        width: 10px;
    }

    #combo-box li {
        cursor: pointer
    }

    .fa {
        font-size: 1.1rem
    }

    #input-search {
        display: block;
        border-bottom: 1px solid gray;
        border-left: unset;
        border-right: unset;
        border-top: unset;
        width: 100%;
        margin-bottom: 10px;
    }

    .brand-select {
        line-height: 35px;
        padding: 0px 10px;
        border-radius: 5px;
        background-color: #358fdc;
        color: white;
        margin-top: 2px;
        margin-bottom: 2px;
        transition: all 0.3s;
    }

    #categories {
        cursor: pointer;
        user-select: none
    }

    #categories > li {
        position: relative;
    }

    #categories > li ul > li {
        position: relative;
    }

    #area {
        text-align: right
    }

    #price {
        text-decoration-line: line-through;
        display: inline-block;
        float: left;
        font-size: 14px;
        margin-right: 12px;
    }

    .price {
        display: inline-block;
        float: right;
    }

    .pro_name {
        line-height: 28px;
    }

    .nav-link {
        padding: 5px 10px !important;
    }

    .dropdown-menu-right, .dropdown-menu {
        text-align: right
    }

    ul {
        padding: unset;
        margin-bottom: 3px;
    }

    .remove-breadcrumb {
        color: #dc3545;
        font-weight: bold;
        font-size: 16px;
        cursor: pointer;
        margin-right: 10px;
        margin-top: 3px;
    }
</style>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/admin/product/edit.blade.php ENDPATH**/ ?>