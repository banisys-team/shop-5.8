<?php $__env->startSection('content'); ?>
    <div class="container mt-5 pb-5 relative" id="area">
        <span class="add-label">پروفایل سایت</span>
        <div class="row add-row">
            <div class="container-fluid">
                <div class="row mt-3">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">نام سایت</label>
                            <input id="name" type="text" class="form-control"
                                   v-model="form.name">
                        </div>
                    </div>
                    <div class="col-lg-3 my-4 my-lg-0">
                        <div class="form-group">
                            <label for="name_e" class="col-md-12 control-label">تلفن پشتیبانی</label>
                            <input id="name_e" type="text" class="form-control text-left"
                                   v-model="form.tell">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="name_e" class="col-md-12 control-label">شماره همراه</label>
                            <input id="name_e" type="text" class="form-control text-left"
                                   v-model="form.mobile">
                        </div>
                    </div>
                    <div class="col-lg-3 my-4 my-lg-0">
                        <div class="form-group">
                            <label for="name_e" class="col-md-12 control-label">صفحه اینستاگرام</label>
                            <input id="name_e" type="text" class="form-control text-left"
                                   v-model="form.instagram">
                        </div>
                    </div>
                </div>

                <div class="row mt-lg-4">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="name_e" class="col-md-12 control-label">کانال تلگرام</label>
                            <input id="name_e" type="text" class="form-control text-left"
                                   v-model="form.telegram">
                        </div>
                    </div>
                    <div class="col-lg-9 my-4 my-lg-0">
                        <div class="form-group">
                            <label for="address" class="col-md-12 control-label">آدرس</label>
                            <textarea id="address" class="form-control" v-model="form.address" rows="1"
                            ></textarea>
                        </div>
                    </div>
                </div>

                <div class="row mt-lg-4">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="about" class="col-md-12 control-label">توضیحات درباره فروشگاه</label>
                            <textarea id="about" class="form-control" rows="5"
                                      v-model="form.about"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row mt-lg-3">
                    <div class="col-lg-6 my-4 my-lg-0">
                        <div class="form-group relative">

                            <label for="fileImage" id="label_file" class="thumbnail-img">
                                لوگو
                            </label>
                            <input type="file" onchange="readURL(this)" style="display:none;"
                                   id="fileImage"
                                   name="fileImage" @change="onImageChange">
                            <img style="width: 300px" id="blah" class="mr-3" :src="'/images/'+form.image"/>
                        </div>
                    </div>
                    <div class="col-lg-5"></div>
                    <div class="col-lg-1 text-left">
                        <div class="form-group">
                            <button style="border-bottom: solid #0055b1 3px;"
                                    type="button" class="btn btn-primary w-100" @click="formSubmit">ثبت
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        new Vue({
            el: '#area',
            data: {
                form: {
                    name: '',
                    tell: '',
                    mobile: '',
                    instagram: '',
                    telegram: '',
                    address: '',
                    about: '',
                    image: '',
                },
            },
            methods: {
                onImageChange(e) {
                    this.form.image = e.target.files[0];
                },
                fetchSettings() {
                    let vm = this
                    axios.get('/admin/setting/fetch').then(res => {
                        vm.form = res.data
                    })
                },
                formSubmit() {
                    let config = {
                        headers: {'content-type': 'multipart/form-data'},
                        onUploadProgress: (e) => {
                            this.percent = Math.round((e.loaded / e.total) * 100)
                        }
                    }

                    let formData = new FormData
                    formData.append('name', this.form.name)
                    formData.append('tell', this.form.tell)
                    formData.append('mobile', this.form.mobile)
                    formData.append('instagram', this.form.instagram)
                    formData.append('telegram', this.form.telegram)
                    formData.append('address', this.form.address)
                    formData.append('about', this.form.about)
                    formData.append('image', this.form.image)

                    let vm = this
                    axios.post('/admin/setting/store', formData, config)
                        .then(() => {
                            swal.fire(
                                {
                                    text: "با موفقیت ثبت شد.",
                                    icon: "success",
                                    confirmButtonText: 'باشه',
                                }
                            )
                            vm.fetchSettings()
                        })
                },
            },
            mounted() {
                this.fetchSettings()
            }
        })
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(300)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#side_setting").addClass("menu-open")
        $("#side_setting_crud").addClass("active")
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <style>
        .thumbnail-img {
            font-size: 15px;
            font-weight: lighter !important;
            padding: 7px 35px;
            color: #ffffff;
            border-radius: 4px;
            vertical-align: top;
            cursor: pointer;
            background: #dc3545;
            border-bottom: 3px solid #ac1f2d;
        }

        .btn-success {
            color: white !important
        }

        .add-label {
            position: absolute;
            margin-right: 10px;
            top: 83px;
            background-color: #f4f6f9;
            color: #9f9f9f;
        }

        .add-row {
            border: 1px #dedede solid;
            padding: 35px 5px 15px 5px;
            border-radius: 10px
        }

        .pagination {
            margin: auto
        }

        .delete {
            background: #dc3545;
            color: white !important;
            padding: 1px 5px;
            font-size: 13px;
            border-radius: 4px;
        }

        .form-group label {
            font-size: 14px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/admin/setting/crud.blade.php ENDPATH**/ ?>