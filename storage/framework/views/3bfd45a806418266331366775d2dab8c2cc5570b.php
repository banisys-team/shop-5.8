<?php $__env->startSection('content'); ?>
    <div class="container mb-5 mt-lg-4 mt-lg-5 pt-2 pb-5 text-right rtl" id="content">
        <div class="row">
            <?php echo $__env->make('layouts.site.aside', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-12 d-lg-none p-0 mb-3">
                        <p style="color: #ffffff;font-size: 17px;text-align: center;background: #1ac977;padding: 8px 0;">همراه : {{ user.mobile }}</p>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label" for="name">نام و نام خانوادگی</label>
                            <input type="text" class="form-control" id="name" v-model="form.name">
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label" for="name">کد پستی</label>
                            <input type="text" class="form-control" id="name" v-model="form.postal_code">
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">استان</label>
                            <input type="text" class="form-control" v-model="form.state">
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">شهر</label>
                            <input type="text" class="form-control" v-model="form.city">
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">آدرس</label>
                            <textarea type="text" class="form-control" v-model="form.address" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="col-12 d-flex flex-row-reverse">
                        <a class="submit-btn" @click="formSubmit" style="cursor:pointer">ثبت</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        new Vue({
            el: '#content',
            data: {
                form: {
                    name: '',
                    postal_code: '',
                    state: '',
                    city: '',
                    address: '',
                },
                user: [],
                flag: true,
            },
            methods: {
                formSubmit() {
                    let vm = this

                    axios.post('/panel/account/store', this.form).then(() => {
                        swal.fire({
                            text: "پروفایل شما با موفقیت ویرایش شد.",
                            type: "success",
                            confirmButtonText: 'باشه',
                        })
                        vm.fetchUser()
                    })
                },
                fetchUser() {
                    let vm = this
                    axios.get(`/panel/fetch/user`).then(res => {
                        vm.form = res.data
                        vm.user = res.data
                    })
                },
            },
            mounted() {
                this.fetchUser()
            }
        })
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>

        .submit-btn {
            background: #1ac977;
            color: white;
            border: unset;
            padding: 8px 23px 4px;
            border-radius: 5px;
            border-bottom: 3px solid #0d9857;
            font-size: 15px;
        }

        body {
            background: white;
        }

        label {
            color: #6f6f6f;
            font-size: 13px;
            padding-right: 10px;
            margin-bottom: 4px;
        }

        input, textarea {
            font-size: 15px !important
        }

        #side-account {
            color: #1ac977 !important;
            font-weight: bold;
        }
        @media  only screen and (max-width: 768px) {
            .submit-btn {
                width:100%;
                padding: 13px;
                text-align: center;
                border: none;
                border-radius: 10px;
            }

        }
        .swal2-container{
            direction: rtl;
        }
    </style>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.site.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/site/panel/account.blade.php ENDPATH**/ ?>