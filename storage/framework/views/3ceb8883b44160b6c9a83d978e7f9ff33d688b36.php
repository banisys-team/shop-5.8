<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo e(config('app.name')); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="stylesheet" href="/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="/dist/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="/dist/css/custom-style.css">
    <script src="<?php echo e(asset('/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
</head>
<body class="hold-transition sidebar-mini">

<style>
    body {
        background: linear-gradient(to right, #8e9eab, #eef2f3);

    }

    label {
        font-size: 14px;
        color: #8e9eab;
    }
</style>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-4"></div>
        <div class="col-md-4 mx-3"
             style="border: 1px solid #dbdbdb;padding: 18px 12px;border-radius: 10px;background: white">
            <p class="text-center mb-4 font-weight-bold mx-2" style="color: #444444;">.:: ورود مدیر ::.</p>
            <form method="POST" action="<?php echo e(route('admin.auth.loginAdmin')); ?>">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <label for="name" class="col-md-12 control-label">نام کاربری</label>

                    <div class="col-md-12">
                        <input id="email" type="name" class="form-control" name="name" value="<?php echo e(old('name')); ?>" style="text-align: left"
                               required autofocus>

                        <?php if($errors->has('name')): ?>
                            <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group mt-4">
                    <label for="password" class="col-md-12 control-label">کلمه عبور</label>

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control" name="password" style="text-align: left"  required>
                    </div>
                </div>

                <div class="form-group mt-5">
                    <div class="col-md-12" style="text-align: center;">
                        <button type="submit" class="btn btn-primary"
                                style="width: 100%;background: #8e9eab;border: unset">
                            ورود
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>
<?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/admin/auth/login.blade.php ENDPATH**/ ?>