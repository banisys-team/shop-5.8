
<?php $__env->startSection('content'); ?>
<div class="container-fluid my-4" id="content">
    <div class="row">
        <div class="col-12">
            <div class="pending" v-if="pending"></div>
            <div class="scrollme">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <td></td>
                        <th style="width: 6%">اولویت</th>
                        <th style="width: 18%">نام</th>
                        <th style="width: 15%">دسته</th>
                        <th style="width: 15%">برند</th>
                        <th style="width: 10%">قیمت</th>
                        <th style="width: 12%">تاریخ</th>
                        <th style="width: 12%;position: relative">
                            <div style="position: absolute;top: 17px">
                                <span style="font-size: 13px;color: #808080;">مجموع:</span>
                                <span style="font-weight: bold;color: #808080;">{{ products.total }}</span>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="position-relative">
                            <div class="position-relative">
                                <input type="text" class="form-control col-12 d-inline-block" v-model="search.name"
                                       @keyup="checkSearchName"
                                       style="padding: 4px 10px;"
                                       @keyup.enter="searchName" placeholder="جستجو...">
                                <a class="btn btn-success btn-sm btn-search d-none d-lg-block" @click="searchName"
                                   style="border:unset;height: 100%">
                                    <img src="/assets/images/search-white.svg">
                                </a>
                            </div>
                        </td>
                        <td class="position-relative">
                            <button @click="flag=!flag" id="btn-cat">
                                {{ search.catName }}
                                <img id="angle-down" src="/assets/images/arrow-down.svg">
                            </button>
                            <div v-if="flag" id="combo-box">
                                <ul v-if="flag1">
                                    <li id="root" @click="showAllCats">مشاهده همه دسته ها</li>
                                    <li id="root-cat" v-for="root in roots"
                                        @click="fetchChild(root.id,root.name,'root')">
                                        {{ root.name }}
                                        <img class="angle-left" src="/assets/images/left-arrow3.svg">
                                    </li>
                                </ul>

                                <ul v-if="flag2">
                                    <li id="back" @click="back">
                                        <i class="right">⮞</i>
                                        <span style="margin-right: 6px;padding-top: 3px;display: inline-block;">
                                                {{ holder.parentName }}
                                            </span>

                                    </li>
                                    <li id="fix" @click="fixCat">{{ holder.selfName }}</li>
                                    <li class="children-item" v-for="child in children"
                                        @click="fetchChild(child.id,child.name,child.children_recursive.length)">
                                        {{ child.name }}
                                        <img v-if="child.children_recursive.length"
                                             class="angle-left" src="/assets/images/left-arrow3.svg">
                                    </li>
                                </ul>

                            </div>
                        </td>
                        <td class="position-relative">
                            <div class="position-relative">
                                <input type="text" class="form-control col-12 d-inline-block"
                                       style="padding: 4px 10px;"
                                       v-model="search.brand"
                                       @keyup="checkSearchBrand"
                                       @keyup.enter="searchBrand" placeholder="جستجو...">
                                <a class="btn btn-success btn-sm btn-search d-none d-lg-block" @click="searchBrand"
                                   style="border:unset;height: 100%">
                                    <img src="/assets/images/search-white.svg">
                                </a>
                            </div>
                        </td>
                        <td class="position-relative"></td>
                        <td class="position-relative">
                            <div class="position-relative">
                                <input type="text" class="form-control col-12 d-inline-block text-center ltr"
                                       style="padding: 4px 10px;"
                                       v-model="search.shamsi_c"
                                       @keyup="checkSearchShamsi"
                                       @keyup.enter="searchShamsi_c">
                                <a class="btn btn-success btn-sm btn-search d-none d-lg-block"
                                   @click="searchShamsi_c"
                                   style="border:unset;height: 100%">
                                    <img src="/assets/images/search-white.svg">
                                </a>
                            </div>
                        </td>
                        <td>
                            <a v-if="recharge" @click="rechargeShow" class="btn btn-warning btn-sm">نیاز به شارژ</a>
                            <a v-if="!recharge" @click="seeAll" class="btn btn-default btn-sm">مشاهده همه</a>
                        </td>
                    </tr>
                    <tr v-for="(product,index) in products.data" :id=`tr${product.id}`
                        :class="statusClass(product.status)"
                        @click="selectRow(product.id)">
                        <td>
                            <img :src=`/images/product/${product.image}` style="width: 100px">
                        </td>
                        <td>
                            <input type="number"
                                   style="direction: ltr;padding: 0 0 0 8px;width:70px;border: 1px solid #d0d0d0;text-align: center"
                                   v-model="product.priority"
                                   @keyup.enter="priorityUpdate(product.id,$event)">
                        </td>
                        <td>{{product.name}}</td>
                        <td>{{product.cats[0]?.name}}</td>
                        <td>
                            <span v-if="product.brand">{{product.brand.name_f}}</span>
                            <span v-else>...</span>
                        </td>

                        <td @dblclick="editPrice(product.id,product.price,product.discount)">
                            <span :id="'price'+product.id">
                                {{numberFormat(product.price)}}
                            </span>

                            <div :class="'input'+product.id" class="display-none">
                                <span style="color: #7d7d7d">قیمت پایه :</span>
                                <input type="number" :id="'inputPrice'+product.id"
                                       style="direction: ltr;padding: 0 0 0 8px;width: 100%;border: 1px solid #d0d0d0;"
                                       v-model="product.price" @keyup="changePrice(product.id,index)"
                                       @keyup.enter="SubmitEditPrice(product.id,index)">
                                <br><br>
                                <span style="color: #7d7d7d">قیمت پس از تخفیف :</span>
                                <input @keyup="calDiscount(index)" v-model="form.afterDiscount"
                                       type="number"
                                       style="direction: ltr;padding: 0 0 0 8px;width: 100%;border: 1px solid #d0d0d0;"
                                       @keyup.enter="SubmitEditPrice(product.id,index)">
                                <br><br>
                                <span style="color: #7d7d7d">تخفیف :</span>
                                <input type="number" :id="'inputDiscount'+product.id"
                                       style="direction: ltr;padding: 0 0 0 8px;width: 100%;border: 1px solid #d0d0d0;"
                                       @keyup="calDiscountPrice2(product.id)"
                                       v-model="product.discount"
                                       @keyup.enter="SubmitEditPrice(product.id,index)">
                            </div>

                        </td>

                        <td>{{product.shamsi_c}}</td>
                        <td>
                            <a class="btn-exist d-inline-block mb-1 mb-lg-0" :href=`/admin/exist/set/${product.id}`
                               target="_blank">
                                موجودی
                            </a>

                            <a :href=`/admin/product/edit/${product.id}` target="_blank" style="font-size: 20px;">
                                <img src="/assets/images/edit.svg" style="width:20px;margin-left: 5px">
                            </a>
                            <a @click="confirmDelete(product.id)"
                               style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                ✖
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div v-if="!products.data.length" class="alert alert-warning text-center"
                     style="border-color: #edb10066;background-color: #ffc1072e!important;">
                    محصولی یافت نشد.
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <pagination :limit="1" :data="products" @pagination-change-page="fetchProducts"
                    style="margin:auto"></pagination>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
    let vm = new Vue({
        el: '#content',
        data: {
            pending: 0,
            flagPrice: true,
            flagPriceInput: false,
            pluss: true,
            pluss2: false,
            fflag: false,
            fflag2: false,
            fflag3: false,
            showModal: false,
            categories: [],
            search: {
                name: '',
                brand: '',
                shamsi_c: '',
                catName: 'انتخاب کنید...',
                price: '',
            },
            description: '',
            products: [],
            //tree
            holder: {
                selfName: 'ریشه',
                selfId: '',
                parentName: '',
                parentId: '',
                grandName: '',
                grandId: '',
            },
            flag: false,
            flag1: true,
            flag2: false,
            roots: [],
            children: [],
            form: {
                cat: 'انتخاب کنید...',
                afterDiscount: '',
            },
            holderPriceId: '',
            holderPrice: '',
            holderDiscount: '',
            recharge: 1,
        },
        methods: {
            seeAll() {
                this.fetchProducts()
                this.recharge = 1
            },
            rechargeShow(page = 1) {
                this.recharge = 0
                axios.get('/admin/product/recharge/fetch?page=' + page).then(res => {
                    vm.products = res.data
                })
            },
            statusClass(status) {
                if (status === 2) return 'orange'
            },
            priorityUpdate(id, event) {
                axios.post('/admin/product/priority/update', {
                    product_id: id,
                    priority: event.target.value
                }).then(() => {
                    swal.fire({
                        type: "success",
                        position: 'center',
                        icon: 'success',
                        title: ' با موفقیت ثبت شد.',
                    })
                    vm.fetchProducts()
                })
            },
            showAllCats() {
                this.fetchProducts()
                this.search.catName = 'انتخاب کنید...'
            },
            checkSearchShamsi() {
                if (this.search.shamsi_c.length === 0) {
                    this.fetchProducts()
                }
            },
            checkSearchPrice() {
                if (this.search.price.length === 0) {
                    this.fetchProducts()
                }
            },
            checkSearchName() {
                if (this.search.name.length === 0) {
                    this.fetchProducts()
                }
            },
            checkSearchBrand() {
                if (this.search.brand.length === 0) {
                    this.fetchProducts()
                }
            },
            calDiscount(index) {
                if (this.products.data[index].price == '') {
                    swal.fire(
                        {
                            text: 'لطفا ابتدا قیمت پایه را وارد کنید .',
                            type: 'warning',
                            confirmButtonText: 'باشه',
                        }
                    );
                    this.form.afterDiscount = '';
                } else {
                    per = this.products.data[index].price / 100;

                    x = this.form.afterDiscount / per;
                    y = 100 - x;
                    this.products.data[index].discount = Math.round(y);
                }
            },
            calDiscountPrice(price, discount) {
                per = price / 100;
                x = 100 - discount;
                y = x * per;

                this.form.afterDiscount = Math.round(y);
            },
            calDiscountPrice2(id) {
                per = $(`#inputPrice${id}`).val() / 100;
                x = 100 - $(`#inputDiscount${id}`).val();
                y = x * per;

                this.form.afterDiscount = Math.round(y);
            },
            changePrice(id, index) {
                this.form.afterDiscount = null;
                this.products.data[index].discount = null;
            },
            editPrice(id, price, discount) {
                this.holderPrice = price;
                this.holderDiscount = discount;

                $(`.input${this.holderPriceId}`).addClass("display-none");
                $(`#price${this.holderPriceId}`).removeClass("display-none");

                $(`#price${id}`).addClass("display-none");
                $(`.input${id}`).removeClass("display-none");
                $(`#inputPrice${id}`).select();

                this.holderPriceId = id;

                this.calDiscountPrice(price, discount);
            },
            SubmitEditPrice(id, index) {
                if (this.products.data[index].discount == null) {
                    swal.fire(
                        {
                            text: "تخفیف یا قیمت پس از تخفیف را وارد کنید .",
                            type: "warning",
                            confirmButtonText: 'باشه',
                        }
                    );
                    return false;
                }

                let formData = new FormData();
                formData.append('id', id);
                formData.append('price', $(`#inputPrice${id}`).val());
                formData.append('discount', $(`#inputDiscount${id}`).val());

                axios.post('/admin/product/edit/price', formData)
                    .then(function (response) {
                        swal.fire(
                            {
                                type: "success",
                                position: 'center',
                                icon: 'success',
                                title: ' با موفقیت ثبت شد !',
                                showConfirmButton: false,
                                timer: 2000
                            }
                        );
                    });
            },
            fetchProducts(page = 1) {
                axios.get('/admin/product/all/fetch?page=' + page).then(res => {
                    vm.products = res.data
                    vm.flag = false
                    vm.form.cat = 'انتخاب کنید...'
                })
            },
            toggleFlag() {
                if (this.pluss === false) {
                    this.pluss = true;
                } else {
                    if (this.pluss === true) {
                        this.pluss = false;
                    }
                }
                if (this.pluss2 === false) {
                    this.pluss2 = true;
                } else {
                    if (this.pluss2 === true) {
                        this.pluss2 = false;
                    }
                }
                if (this.fflag === false) {
                    this.fflag = true;
                } else {
                    if (this.fflag === true) {
                        this.fflag = false;
                    }
                }
            },
            searchName(page = 1) {
                this.search.catName = 'انتخاب کنید...'
                this.search.brand = ''
                this.search.price = ''
                this.search.shamsi_c = ''
                if (this.search.name.length > 0) {
                    axios.get('/admin/product/all/search?page=' + page, {
                        params: {
                            name: this.search.name
                        }
                    }).then(res => {
                        vm.products = res.data
                    })
                }
            },
            searchCat(page = 1) {
                this.search.brand = ''
                this.search.price = ''
                this.search.shamsi_c = ''
                axios.get('/admin/product/all/search?page=' + page, {
                    params: {
                        cat_id: this.holder.selfId,
                    }
                }).then(res => {
                    vm.products = res.data
                })
            },
            searchBrand(page = 1) {
                this.search.name = ''
                this.search.catName = 'انتخاب کنید...'
                this.search.price = ''
                this.search.shamsi_c = ''
                if (this.search.brand.length > 0) {
                    axios.get('/admin/product/all/search?page=' + page, {
                        params: {
                            brand: this.search.brand,
                        }
                    }).then(res => {
                        vm.products = res.data
                    })
                }
            },
            searchShamsi_c(page = 1) {
                this.search.name = ''
                this.search.catName = 'انتخاب کنید...'
                this.search.brand = ''
                this.search.price = ''
                if (this.search.shamsi_c.length > 0) {
                    axios.get('/admin/product/all/search?page=' + page, {
                            params: {
                                shamsi_c: this.search.shamsi_c
                            }
                        }
                    ).then(res => {
                        vm.products = res.data
                    })
                }
            },
            confirmDelete(id) {
                swal.fire({
                    text: "آیا از پاک کردن اطمینان دارید ؟",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'بله',
                    cancelButtonText: 'لغو',
                }).then((result) => {
                    if (result.value) {
                        vm.pending = 1
                        vm.delete(id)
                    }
                })
            },
            async delete(id) {
                await axios.delete(`/admin/product/delete/${id}`).then(() => {
                    vm.fetchProducts()
                    vm.pending = 0
                })
            },
            numberFormat(number) {
                return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            clickCatBtn() {
                if (this.flag === false) {
                    this.flag = true;
                } else if (this.flag === true) {
                    this.flag = false
                }
            },
            fetchRootCat() {
                let data = this;
                axios.get(`/admin/brand/fetch/cat/root`).then(res => {
                    data.roots = res.data;
                });
                this.holder.parentName = 'ریشه';
                this.holder.parentId = '';
                this.holder.grandName = 'ریشه';
                this.holder.grandId = '';
            },
            fetchChild(id, name, length) {
                this.flagPriority = false
                this.holder.grandName = this.holder.parentName
                this.holder.grandId = this.holder.parentId

                this.holder.parentName = this.holder.selfName
                this.holder.parentId = this.holder.selfId

                this.holder.selfName = name
                this.holder.selfId = id

                if (length === 0) {
                    this.fixCat()
                    this.holder.selfName = 'ریشه'
                    // this.holder.selfId = ''
                    this.holder.parentName = ''
                    this.holder.parentId = ''
                    this.holder.grandName = ''
                    this.holder.grandId = ''
                    this.flag1 = true
                    this.flag2 = false
                    this.fetchCategories()
                    return
                }

                this.flag1 = false
                this.flag2 = true


                this.children = this.categories.filter(cat => cat.parent === id)
            },
            back() {
                let http = axios.create({baseURL: 'http://localhost:8000/api/admin'})
                http.defaults.withCredentials = true

                if (this.holder.parentName === 'ریشه') {
                    this.flag1 = true
                    this.flag2 = false
                    this.holder.selfName = 'ریشه'
                    this.holder.selfId = ''
                    this.holder.parentName = ''
                    this.holder.parentId = ''
                    this.holder.grandName = ''
                    this.holder.grandId = ''
                } else {
                    this.children = this.categories.filter(cat => cat.parent === this.holder.parentId)

                    this.holder.selfName = this.holder.parentName
                    this.holder.selfId = this.holder.parentId

                    this.holder.parentName = this.holder.grandName
                    this.holder.parentId = this.holder.grandId

                    let x = this.categories.filter(cat => cat.id === this.holder.grandId)
                    if (x[0]) {
                        let y = this.categories.filter(cat => cat.id === x[0].parent)
                        if (y[0]) {
                            this.holder.grandName = y[0].name
                            this.holder.grandId = y[0].id
                        } else {
                            this.holder.grandName = 'ریشه'
                            this.holder.grandId = ''
                        }
                    }
                }
            },
            fixCat() {
                this.search.catName = this.holder.selfName
                this.flag = false
                this.searchCat()
            },
            selectRow(id) {
                $(this.holderClass).removeClass('select-row');
                this.holderClass = `#tr${id}`;
                $(`#tr${id}`).addClass('select-row');
            },
            fetchCategories() {
                axios.get(`/admin/category/fetch`).then(res => {
                    vm.categories = res.data
                    vm.roots = res.data.filter(cat => cat.parent === null)
                })
            },
            fixRoot() {
                this.form.parent = 'ریشه'
                this.flag = false
            },
        },
        mounted() {
            this.fetchProducts()
            this.fetchCategories()
        },
    })
</script>
<script>
    $("#side_product").addClass("menu-open");
    $("#side_product_index").addClass("active");
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<style>
    .pending {
        background: white;
        width: 100%;
        height: 100vh;
        position: fixed;
        z-index: 99999;
        opacity: .4;
    }

    #fix {
        color: black;
        font-weight: bold;
        background-color: #d1d1d1;
        padding-right: 6px;
        border-radius: 6px
    }

    .angle-left {
        float: left;
        margin-top: 14px;
        color: #636363;
        width: 10px;
    }

    #angle-down {
        float: left;
        width: 11px;
        margin: 5px 2px 0px 5px;
    }

    .btn-exist {
        background-color: #3490dc;
        padding: 1px 5px 4px 5px;
        color: white;
        border-radius: 5px;
        font-size: 12px;
        margin-left: 5px;
    }

    .btn-exist:hover {
        color: white
    }

    .value-center {
        width: 90% !important;
    }

    .value-center::placeholder {
        text-align: right;
        direction: rtl
    }

    .value-center:not(placeholder_shown) {
        text-align: center;
        direction: ltr
    }


    #btn-cat {
        position: relative;
        background-color: white;
        border: 1px solid #ced4da;
        width: 180px;
        padding: 5px 8px 3px 8px;
        border-radius: 5px;
        text-align: right;
        color: #484848;
    }

    #btn-cat:focus {
        outline: unset;
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        box-shadow: inset 0 0 0 transparent, 0 0 0 0.2rem rgb(0 123 255 / 25%);
    }

    .btn-search {
        position: absolute;
        top: 0px;
        left: 0px;
        border-radius: 5px 0 0 5px;
        padding: 7px 6px;
    }

    .btn-search img {
        width: 15px;
    }

    .display-none {
        display: none
    }


    .left {
        float: right;
        font-weight: bold;
        font-size: 22px;
        margin: 3px -2px 0px 5px;
    }

    #angle-left {
        float: left;
        color: #636363;
        font-weight: bold;
        font-size: 22px;
    }

    .modal-mask {
        position: fixed !important;
        z-index: 9998 !important;
        top: 0 !important;
        left: 0 !important;
        width: 82.5% !important;
        height: 100vh !important;
        background-color: rgba(0, 0, 0, .5) !important;
        display: table !important;
        transition: opacity .3s ease !important;
    }

    .modal-content {
        max-height: calc(100vh - -3.5rem) !important;
        height: 100vh
    }

</style>

<style>
    li {
        list-style: none
    }

    #sss {
        position: absolute;
        list-style: none;
        z-index: 99;
        background-color: white;
        padding: 5px 15px;
        width: 210px;
        line-height: 35px;
        box-shadow: 0px 10px 21px 0px rgba(0, 0, 0, 0.75);
    }


    #sss li {
        cursor: pointer
    }

    .fa {
        font-size: 1.1rem
    }

    .btn-success {
        background-color: #b5b5b5;
        border-color: #b5b5b5;
    }

    .select-row {
        border: 2px #6cafff dotted;
    }

    .scrollme {
        overflow-x: auto;
        min-height: 100vh;
    }

    #combo-box {
        position: absolute;
        list-style: none;
        top: 45px;
        z-index: 99;
        background-color: white;
        padding: 5px 10px;
        width: 100%;
        line-height: 35px;
        border: 1px solid #c8c8c8;
        overflow-x: hidden;
        max-height: 350px;
        border-radius: 0 0 8px 8px;
    }

    #combo-box li {
        cursor: pointer
    }

    .orange {
        border-right: 10px solid #ffa707bf;
    }
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/admin/product/index.blade.php ENDPATH**/ ?>