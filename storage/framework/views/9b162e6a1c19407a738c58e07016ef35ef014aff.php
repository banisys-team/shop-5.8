<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>بانی سیستم</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="stylesheet" href="/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="/dist/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="/dist/css/custom-style.css">
    <script src="<?php echo e(asset('/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
    <?php echo $__env->yieldContent('style'); ?>
    <style>
        .sidebar .nav-link.active {
            background-color: #3490dc !important;
            font-weight: 10 !important;
            border-radius: 0 5px 5px 0 !important;
        }

        .main-sidebar, .main-sidebar:before {
            width: 220px
        }

        .nav-pills .nav-link:not(.active):hover {
            color: white;
        }

        .nav-pills .nav-link {
            color: #c2c7d0;
        }

        .nav-sidebar .fa {
            color: #ababab
        }
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <?php echo $__env->make('layouts.admin.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('layouts.admin.aside', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="content-wrapper">
        <div class="content-header" style="padding: 0">
            <div class="container-fluid" style="padding: 0">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <?php echo $__env->yieldContent('breadcrumb'); ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $__env->yieldContent('content'); ?>
    </div>
</div>
<script src="<?php echo e('/js/app.js'); ?>"></script>

<?php echo $__env->yieldContent('script'); ?>
<script src="<?php echo e(asset('dist/js/adminlte.min.js')); ?>"></script>
<script src="<?php echo e(asset('dist/js/demo.js')); ?>"></script>
</body>
</html>
<?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/layouts/admin/app.blade.php ENDPATH**/ ?>