<?php $__env->startSection('content'); ?>
    <div class="container-fluid rtl text-right mt-4 mt-lg-5 pb-5 pb-lg-0 mb-5" id="content">
        <div class="d-flex justify-content-center">
            <div class="col-lg-7" style="background: white;border-radius: 8px;">
                <section class="row pb-3 py-lg-5 px-lg-3" v-show="flag1"
                         style="border: 1px solid #dedede;border-radius: 10px;">
                    <div
                        class="levels d-flex flex-row justify-content-between align-items-center col-11 col-lg-9 mx-auto mt-4 mt-lg-2 mb-lg-3 px-0 px-lg-5">
                        <div
                            class="cart-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto current-lev px-0">
                            <a href="/cart"
                               class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center  current-lev col-lg-auto px-0">
                                <img src="/assets/images/shipping-lev-1.svg" alt="" class="px-4 px-lg-1">
                                <span class="mx-auto d-block d-lg-inline mr-lg-2 mt-2 mt-lg-0">سبد خرید</span>
                            </a>
                        </div>
                        <div class="line-lev current-line-lev"></div>
                        <div
                            class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center col-3 current-lev col-lg-auto px-0">
                            <a
                                class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center  current-lev col-lg-auto px-0">
                                <img src="/assets/images/shipping-lev-2.svg" alt="" class="px-4 px-lg-1">
                                <span class="mx-auto d-block d-lg-inline mr-lg-2 mt-2 mt-lg-0">اطلاعات ارسال</span>
                            </a>
                        </div>
                        <div class="line-lev"></div>
                        <div
                            class="pay-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto px-0">
                            <a @click="payment" style="cursor: pointer"
                               class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center  current-lev col-lg-auto px-0">
                                <img src="/assets/images/shipping-lev-3.svg" style="filter: grayscale(1);opacity: .6;"
                                     class="px-4 px-lg-1">
                                <span class="mx-auto d-block d-lg-inline mr-lg-2 mt-2 mt-lg-0" style="opacity: .5">پرداخت</span>
                            </a>
                        </div>
                    </div>

                    <div class="col-12 mb-4 mb-lg-5 mt-5 pt-3">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label" for="name">نام و نام خانوادگی</label>
                            <input type="text" class="form-control py-4" id="name" v-model="form.name">
                        </div>
                    </div>

                    <div class="col-12 mb-4 mb-lg-5" v-if="userId == 1">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label" for="mobile">موبایل</label>
                            <input type="text" class="form-control py-4" id="mobile" v-model="form.mobile">
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 mb-4 mb-lg-5">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">استان</label>
                            <input type="text" class="form-control py-4" v-model="form.state">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 mb-4 mb-lg-5">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">شهر</label>
                            <input type="text" class="form-control py-4" v-model="form.city">
                        </div>
                    </div>
                    <div class="col-12 mb-4 mb-lg-5">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">آدرس</label>
                            <textarea type="text" class="form-control" rows="5" v-model="form.address"></textarea>
                        </div>
                    </div>
                    <div class="col-12 mb-4 mb-lg-5">
                        <div class="form-group" style="margin-bottom: 4px">
                            <label class="control-label">توضیحات</label>
                            <textarea type="text" class="form-control" rows="3" v-model="form.description"></textarea>
                        </div>
                    </div>
                    <div class="col-12" v-if="userId != 1">
                        <div class="p-4"
                             style="line-height: 28px;background: #f5f5f5;border-radius: 6px;color: #656565;">
                            <p>
                                چنانچه قصد خرید مجدد در روزهای آینده را دارید ،
                                می توانید گزینه
                                <span style="color: #1ac977;font-weight: bold;"> رزرو شود </span>
                                را انتخاب نمایید تا مجموع
                                اجناس چند سفارش به صورت یکجا برای شما ارسال گردد .<br> این کار باعث کاهش هزینه ارسال می
                                شود . ( فقط یک هزینه ارسال روی سفارش آخر اعمال می شود )
                            </p>
                            <div class="mt-4 text-center">
                                <select v-model="form.reserve"
                                        style="border:1px solid #ddd;border-radius:5px;padding:8px;color: #3a3a3a;font-size: 14px;">
                                    <option value="0" v-if="!reserveExist">در اسرع وقت ارسال شود</option>
                                    <option value="1">رزرو شود</option>
                                    <option value="2" v-if="reserveExist">همراه با سفارشات قبلیم بفرست</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 d-flex flex-row-reverse mt-4">
                        <button @click="payment"
                                class="shipping-continue-btn"
                                style="background: #1ac977;color: white;border: unset;padding: 10px 35px;border-radius: 8px;font-size: 15px;">
                            ادامه
                            <img style="width: 11px;filter: grayscale(0);margin-right: 4px;"
                                 src="/assets/images/left-arrow-white.svg" alt="">
                        </button>
                    </div>
                </section>
                <section class="row pb-3 py-lg-5 px-lg-0" v-show="flag2"
                         style="border: 1px solid #dedede;border-radius: 10px;">
                    <div
                        class="levels d-flex flex-row justify-content-between align-items-center col-11 col-lg-9 mx-auto mt-4 mt-lg-2 mb-lg-3 px-0 px-lg-5">
                        <div
                            class="cart-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto current-lev px-0">
                            <a href="/cart"
                               class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center  current-lev col-lg-auto px-0">
                                <img src="/assets/images/shipping-lev-1.svg" alt="" class="px-4 px-lg-1">
                                <span class="mx-auto d-block d-lg-inline mr-lg-2 mt-2 mt-lg-0">سبد خرید</span>
                            </a>
                        </div>
                        <div class="line-lev current-line-lev"></div>
                        <div
                            class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto current-lev px-0">
                            <a class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center  current-lev col-lg-auto px-0"
                               @click="back" style="cursor:pointer;">
                                <img src="/assets/images/shipping-lev-2.svg" alt="" class="px-4 px-lg-1">
                                <span class="mr-lg-2 mt-2 mt-lg-0">اطلاعات ارسال</span>
                            </a>

                        </div>
                        <div class="line-lev current-line-lev"></div>
                        <div
                            class="pay-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto current-lev px-0">
                            <a class="info-lev d-flex flex-column flex-lg-row justify-content-center align-items-lg-center  current-lev col-lg-auto px-0">
                                <img src="/assets/images/shipping-lev-3.svg" alt="" class="px-4 px-lg-1">
                                <span class="mr-lg-2 mt-2 mt-lg-0">پرداخت</span>
                            </a>

                        </div>
                    </div>

                    <div class="col-lg-6 mt-4 pt-lg-4 mini-factor px-0" style="font-size: 15px">
                        <div class="row my-3">
                            <div class="col-6">
                                <p class="text-left" style="color: #828282;">قیمت کالاها :</p>
                            </div>
                            <div class="col-6">
                                <p class="text-right" style="color: #828282;">
                                    {{ numberFormat(Math.round(finalTotal)) }}
                                    <span style="font-size: 12px">تومان</span>
                                </p>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-6">
                                <p class="text-left" style="color: #828282;">هزینه ارسال :</p>
                            </div>
                            <div class="col-6">
                                <p class="text-right" style="color: #828282;"> {{ numberFormat(delivery) }}
                                    <span style="font-size: 12px">تومان</span>
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                
                                <p class="text-left" style="color: #828282;">ارزش افزوده :</p>

                            </div>
                            <div class="col-6">
                                
                                
                                
                                <p class="text-right" style="color: #828282;"> {{ numberFormat(Math.round(vat)) }}
                                    <span style="font-size: 12px">تومان</span>
                                </p>
                            </div>
                        </div>
                        <hr class="mx-5 my-3">
                        <div class="row">
                            <div class="col-6">
                                <p class="text-left mb-0" style="color: black;font-weight: bold">مبلغ قابل پرداخت
                                    :</p>
                            </div>
                            <div class="col-6" style="color: black;font-weight: bold">
                                <p class="text-right mb-0"> {{ numberFormat(Math.round(finalTotal) +
                                    Math.round(delivery) + Math.round(vat)) }}
                                    <span style="font-size: 13px">تومان</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 px-0 mt-3 mt-lg-4" style="font-size: 15px">
                        <div class="row mt-4 mt-lg-4">
                            <div class="col-12 text-center">
                                <div @click="bank('zarinpal')" :class="{'zarinpal': zarinpalFlag}"
                                     style="cursor: pointer">
                                    <img src="/images/zarinpal.png" style="width: 80px">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 mt-lg-5 px-3 px-lg-0">
                            <div class="col-12 col-lg-6 text-center">
                                <button @click="back"
                                        class="back-button col-12 col-lg-auto mt-3 mt-lg-0">

                                    <img src="/assets/images/left-arrow2.svg" style="margin-left: 2px">
                                    <span>مرحله قبل</span>
                                </button>

                            </div>
                            <div class="col-12 col-lg-6">
                                <button v-if="userId == 1" @click="manuallySubmit" class=""
                                        style="background: #1ac977;color: white;border: unset;padding: 8px 23px 4px;border-radius: 8px;border-bottom: 3px solid #0d9857;font-size: 15px;">
                                    ثبت دستی
                                </button>

                                <button v-else @click="submitEpay" class="col-12 col-lg-auto mt-4 mt-lg-0"
                                        style="background: #1ac977;color: white;padding: 10px 50px;border-radius: 8px;border:unset;font-size: 15px;">
                                    پرداخت
                                </button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        new Vue({
            el: '#content',
            data: {
                form: {
                    name: '',
                    state: '',
                    city: '',
                    address: '',
                    description: '',
                    distancePrice: '',
                    reserve: '0',
                    mobile: '',
                },
                flag1: true,
                flag2: false,
                carts: [],
                delivery: 0,
                deliveryMin: 0,
                deliveryMax: 0,
                deliveryNumber: 0,
                chosenBank: 'zarinpal',
                zarinpalFlag: false,
                sadadFlag: true,
                holderDelivery: 0,
                currentOrderNum: 0,
                reserveExist: 0,
                totalWithoutDiscount: 0,
                finalTotal: 0,
                userId: <?php echo auth()->user()->id; ?>,
            },
            computed: {
                vat() {
                    let per = this.finalTotal / 100
                    return (per * 2)
                }
            },
            methods: {
                validateForm() {
                    if (this.form.name == '') {
                        swal.fire({
                            text: 'نام و نام خانوادگی را وارد کنید',
                            icon: 'warning',
                            confirmButtonText: 'باشه',
                        })
                        return 0
                    }
                    if (this.form.state == '' || this.form.city == '') {
                        swal.fire({
                            text: 'شهر و استان را وارد کنید',
                            icon: 'warning',
                            confirmButtonText: 'باشه',
                        })
                        return 0
                    }
                    if (this.form.address == '') {
                        swal.fire({
                            text: 'آدرس را وارد کنید',
                            icon: 'warning',
                            confirmButtonText: 'باشه',
                        })
                        return 0
                    }
                    return 1
                },
                bank(bank) {
                    this.chosenBank = bank
                    if (bank === 'zarinpal') {
                        this.zarinpalFlag = true
                        this.sadadFlag = false
                    }
                    if (bank === 'sadad') {
                        this.zarinpalFlag = false
                        this.sadadFlag = true
                    }
                },
                async payment() {
                    $("html, body").animate({scrollTop: 100}, "slow");
                    let vm = this
                    if (!this.validateForm()) return
                    if (this.form.reserve == '0') {
                        this.delivery = (parseInt(this.currentOrderNum) > this.deliveryNumber) ? this.deliveryMax : this.deliveryMin
                    }

                    if (this.form.reserve == '1') {
                        this.delivery = 0
                    }
                    if (this.form.reserve == '2') {
                        if (this.delivery !== this.deliveryMax) {
                            await axios.get('/shipping/fetch/order/reserve/num').then((res) => {
                                if (parseInt(vm.currentOrderNum) + parseInt(res.data) > vm.deliveryNumber) {
                                    vm.delivery = this.deliveryMax
                                }
                            })
                        }
                    }
                    this.flag1 = false
                    this.flag2 = true
                },
                back() {
                    $("html, body").animate({scrollTop: 100}, "slow");
                    this.flag2 = false
                    this.flag1 = true
                    window.scrollTo(0, 0)
                },
                deliveryShow() {
                    this.flag1 = false;
                    this.flag2 = true;
                    window.scrollTo(0, 0);

                },
                numberFormat(price) {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                submitEpay() {
                    let vm = this
                    axios.post('/submit/epay', {
                        name: this.form.name,
                        state: this.form.state,
                        city: this.form.city,
                        address: this.form.address,
                        description: this.form.description,
                        reserve: this.form.reserve,
                        delivery: this.delivery,
                        final_total: this.finalTotal,
                        vat: this.vat,
                    }).then(() => {
                        window.location.href = `/order/epay/redirect/${vm.chosenBank}`
                        //   window.location.href = `/order/epay/redirect/zarinpal`
                    })
                },
                reserveExistCheck() {
                    let vm = this
                    axios.get('/reserve/exist/check').then(res => {
                        vm.reserveExist = res.data
                        if (res.data) {
                            vm.form.reserve = 2
                        }

                    })
                },
                formSubmit() {
                    axios.post('/order/store', {
                        name: this.form.name,
                        city: this.form.city,
                        cell: this.form.cell,
                        state: this.form.state,
                        address: this.form.address,
                        distance_price: this.form.distancePrice,
                        delivery_time: this.form.delivery_time,
                        sum_final: this.sum_final,
                    }).then(() => {
                        window.location.href = `/panel/orders`;
                    })
                },
                fetchCart() {
                    let vm = this
                    axios.get('/fetch/cart').then(res => {
                        vm.carts = res.data
                        vm.carts.forEach(cart => vm.currentOrderNum += cart.number)
                        vm.calTotalWithoutDiscount()
                        vm.calTotal()
                    })
                },
                fetchUser() {
                    let vm = this
                    axios.get('/user/fetch').then(res => {
                        vm.form.name = res.data.name
                        vm.form.state = res.data.state
                        vm.form.city = res.data.city
                        vm.form.address = res.data.address
                    })
                },
                calTotalWithoutDiscount() {
                    this.carts.forEach(cart => {
                        this.totalWithoutDiscount += cart.product.price * cart.number
                    })
                },
                calTotal() {
                    this.carts.forEach(cart => {
                        let per = cart.product.price / 100
                        let x = 100 - cart.product.discount
                        let y = per * x
                        this.finalTotal += y * cart.number
                    })
                },
                manuallySubmit() {
                    let vm = this
                    axios.post('/manually/order/submit', {
                        mobile: this.form.mobile,
                        name: this.form.name,
                        state: this.form.state,
                        city: this.form.city,
                        address: this.form.address,
                        description: this.form.description,
                        reserve: this.form.reserve,
                        delivery: this.delivery,
                        final_total: this.finalTotal,
                    }).then(() => {
                        window.location.href = `/admin/order/index`
                        //   window.location.href = `/order/epay/redirect/zarinpal`
                    })
                },
                fetchDeliveryPrice() {
                    let vm = this
                    axios.get('/delivery/price/fetch').then(res => {
                        vm.delivery = parseInt(res.data.min)
                        vm.deliveryMin = parseInt(res.data.min)
                        vm.deliveryMax = parseInt(res.data.max)
                        vm.deliveryNumber = parseInt(res.data.number)
                    })
                },
            },
            async mounted() {
                await this.fetchDeliveryPrice()
                this.fetchCart()
                this.fetchUser()
                this.reserveExistCheck()
                document.getElementById('name').focus()
            }
        })
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>
        .back-button {
            background: #ececec;
            border: unset;
            font-size: 15px;
            padding: 10px 20px;
            border-radius: 8px;
            color: #555555
        }

        label {
            color: #363636;
            font-size: 14px;
            padding-right: 10px;
            margin-bottom: 6px;
        }

        input, textarea {
            font-size: 15px !important
        }

        .mini-factor p {
            margin-bottom: 0px;
        }
    </style>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.site.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/salviasi/shop.banisys.ir/resources/views/site/shipping.blade.php ENDPATH**/ ?>