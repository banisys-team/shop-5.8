<div class="d-none d-lg-block col-3 px-4">
    <div class="col-12 text-right account-menu">
        <li class="mb-4 mt-1 text-center py-2"
            style="direction: ltr;background: #1ac977;color: white;padding: 5px 13px;border-radius:5px;list-style: none;">
            <span style="color: white;">@{{ user.mobile }}</span>
        </li>
        <ul class="pr-2 m-0">
            <li class="mb-4 mt-1 pr-2" id="info-btn" style="direction: ltr">
                <a href="{{url('/panel/account')}}">
                    <span id="side-account" style="color: #6f6f6f;">پروفایل</span>
                </a>
            </li>
            <li class="mb-4 mt-1 pr-2" id="order-btn" style="direction: ltr">
                <a href="{{url('/panel/orders')}}">
                    <span id="side-order" style="color: #6f6f6f;">سفارشات</span>
                </a>
            </li>

            <li class="mb-4 mt-1 pr-2" id="reserve-btn" style="direction: ltr">
                <a href="{{url('/panel/reserve')}}">
                    <span id="side-reserve" style="color: #6f6f6f;">سفارشات رزرو</span>
                </a>
            </li>

            <li class="mt-1 pr-2" style="direction: ltr;height: 45px;">
                <a href="{{url('/logout')}}">
                    <span style="color: #6f6f6f;">خروج</span>
                </a>
            </li>
        </ul>
    </div>
</div>

