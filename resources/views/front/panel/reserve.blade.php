@extends('layouts.front.dress')
@section('content')
    <div class="clearfix container mb-5" id="area" style="max-width: 1140px">
        @include('layouts.front.aside')
        <div class="col-lg-9 float-right pl-0 pr-0  content mt-5">
            <div class="col-md-12 orders pl-0 pr-0   text-center">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">شماره فاکتور</th>
                        <th scope="col">تاریخ</th>
                        <th scope="col">وضعیت</th>
                        <th scope="col">
                            <span style="color:#c40316;font-weight: bold">مجموع :@{{ orders.total }}</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="order in orders">
                        <td>@{{ order.id}}</td>
                        <td>@{{order.shamsi_c}}</td>
                        <td>
                            @{{ fetchStatus(order.status) }}
                        </td>
                        <td>
                            <a @click="factor(order.id)"
                               style="font-size: 20px;background-color: #ffcbc9;padding: 0 10px;border-radius: 8px;">
                                <i class="fa fa-list-alt" style="color: #3a3a3a"></i>
                                <span style="font-size: 15px;vertical-align: middle;">فاکتور</span>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div v-if="flag2"
                     class="alert alert-info" role="alert" style="text-align: center">
                    لیست سفارشات در انبار شما خالی است !
                </div>
            </div>
            <div class="col-12 text-center" v-if="!flag2">
                    <p>کل سفارشات بالا را برایم بفرست</p>
                <a href="/panel/reserve/delivery/zarinpal" class="btn btn-success wanted">
                    پرداخت هزینه ارسال
                </a>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                orders: [],
                user: [],
                form: {
                    image: '',
                },
                flag: true,
                flag2: false,
            },
            methods: {
                fetchReserves() {
                    let _this = this;
                    axios.get('/panel/reserve/fetch').then(res => {
                        _this.orders = res.data
                        _this.emptyOrder()
                    })
                },
                factor(id) {
                    window.open(`/factor/${id}`, '_blank');
                },
                fetchStatus(id) {
                    if (id === 0) {
                        return "در صف بررسی"
                    }
                    if (id === 1) {
                        return "ارسال شد"
                    }
                    if (id === 2) {
                        return "لغو شد"
                    }
                    if (id === 3) {
                        return "آماده ارسال"
                    }
                    if (id === 4) {
                        return "رزرو شد"
                    }
                },
                fetchUser() {
                    let data = this;
                    axios.get(`/panel/fetch/user`).then(res => {
                        data.user = res.data;
                        data.form.image = data.user.image;
                    });
                },
                exit() {
                    this.$refs.formExit.submit();
                },
                emptyOrder() {
                    if (!this.orders.length) {
                        this.flag2 = true
                    }
                },
            },
            mounted: function () {
                this.fetchReserves()
            }
        })
        $("#reserve-btn").addClass('active-menu')
    </script>

@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="/layout/style.css">
    <style>
        .wanted:hover {
            color: white !important;
        }

        .wanted {
            padding: 10px 12px;
            font-size: 14px !important;
            border-radius: 6px;
        }

        .history .col-12 {
            background-color: unset;
            border-radius: unset;
            float: unset;
            text-align: unset;
        }

        .btn {
            height: unset;
            line-height: unset;
        }

        #panel_side > li {
            text-align: right !important;
        }

        .admin {
            background-color: #ffef636b !important
        }

        .active-menu .fa {
            color: #c40316
        }

        .active-menu {
            background-color: aliceblue;
        }

        .active-menu span {
            font-weight: bold;
            color: #c40316 !important;
        }

        #panel_side li:hover a, #panel_side li:hover a span {
            color: #123b66 !important;
        }

        #panel_side li a {
            color: #c9c9c9
        }

        .account-img {
            display: none
        }
    </style>
@endsection

