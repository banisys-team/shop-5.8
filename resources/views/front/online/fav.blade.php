@extends('layouts.front.online')
@section('content')
    <div id="area">
        <div class="breadcrumbs_area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb_content">
                            <ul>
                                <li><a href="index.html">خانه</a></li>
                                <li>لیست علاقه‌مندی‌ها</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="wishlist_page_bg">
            <div class="container">
                <div class="wishlist_area">
                    <div class="wishlist_inner">
                        <form action="#">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table_desc wishlist">
                                        <div class="cart_page table-responsive">
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th class="product_remove">حذف</th>
                                                    <th class="product_thumb">تصویر</th>
                                                    <th class="product_name">محصول</th>
                                                    <th class="product-price">قیمت</th>
                                                    <th class="product_quantity">وضعیت انبار</th>
                                                    <th class="product_total">افزودن به سبد</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <template v-for="(fav, index) in favs.data">
                                                    <tr>
                                                        <td class="product_remove"><a @click="deleteFav(fav.id)">×</a></td>
                                                        <td class="product_thumb">
                                                            <img :src="'/images/product/'+fav.product.image" style="width: 120px">
                                                        </td>
                                                        <td class="product_name">
                                                            <a :href="'/detail/'+fav.product.slug">@{{ fav.product.name }}</a>
                                                        </td>
                                                        <td class="product-price">
                                                            @{{ calculateDiscount(fav.product.price,fav.product.discount) }} تومان
                                                        </td>
                                                        <td class="product_quantity">موجود در انبار</td>
                                                        <td class="product_total">
                                                            <a @click.prevent="detail(fav.product.slug)">خرید محصول</a>
                                                        </td>


                                                    </tr>
                                                </template>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="wishlist_share">
                                <h4>اشتراک گذاری در:</h4>
                                <ul>
                                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                    <li><a href="#"><i class="fa fa-telegram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var app = new Vue({
            el: '#area',
            data: {
                favs: {},
                cal_discount: '',
            },
            methods: {
                fetchFavs() {
                    axios.get(`/fetch/favs`).then(res => {
                        this.$data.favs = res.data;
                    });
                },
                calculateDiscount(price, discount) {
                    onePercent = price / 100;
                    difference = 100 - discount;
                    total = difference * onePercent;
                    this.cal_discount = Math.round(total);
                    return this.cal_discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                detail(slug) {
                    window.location.href = `/detail/${slug}`;
                },
                exist(num) {
                    if (num <= 0) {
                        return false;
                    } else {
                        return true;
                    }
                },
                deleteFav(id) {
                    let obj = this;
                    swal.fire({
                        text: "آیا از پاک کردن اطمینان دارید ؟",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'بله',
                        cancelButtonText: 'لغو',
                    }).then((result) => {
                        if (result.value) {
                            axios.get(`/fav/delete/${id}`).then(res => {
                                obj.fetchFavs();
                            });
                        }
                    });
                },
            },
            mounted: function () {
                this.fetchFavs();
            }
        })
    </script>
@endsection

@section('style')

@endsection

