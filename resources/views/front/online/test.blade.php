@extends('layouts.front.online')
@section('content')
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div id="area">
        <div class="d-flex flex-column bd-highlight mb-3">
            <template v-for="(category,index) in categories">
                <div class="p-2 bd-highlight">@{{ category.name }}</div>

                <div class="d-flex flex-column bd-highlight mb-3">
                    <template v-for="(category,index) in category.children_recursive">
                        <div class="p-2 bd-highlight">@{{ category.name }}</div>
                    </template>
                </div>

            </template>
        </div>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                categories: [],
            },
            methods: {
                fetchCategories() {
                    let data = this;
                    axios.get('/admin/category/fetch/test')
                        .then(res => {
                            data.categories = res.data;
                            console.log(data.categories);
                        });
                },
            },
            mounted: function () {
                this.fetchCategories();
            },
        });
    </script>
@endsection

@section('style')

@endsection

