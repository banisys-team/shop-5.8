@extends('layouts.front.online')
@section('content')

    <div id="area">
        <div class="container-fluid px-0 mt-4">
            <div class="row">
                <div class="col-md-12 px-0">
                    <div id="carouselExampleIndicators" class="carousel slide mt-1" data-ride="carousel">
                        <ol class="carousel-indicators">

                            <li v-for="(item,index) in slider"
                                data-target="#carouselExampleIndicators"
                                data-slide-to="index" :class="ifActive(index)"></li>

                        </ol>
                        <div class="carousel-inner">

                            <div v-for="(item,index) in slider" :class="[(index===0) ? 'active' : '', 'carousel-item']">
                                <a :href="item.url">
                                    <img class="d-block w-100"
                                         :src="'/images/slider/'+item.image">
                                </a>

                            </div>

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <div class="home_section_bg pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-12 order-lg-2">
                        <div class="product_area">
                            <div class="row">
                                <div class="col-12">
                                    <div class="product_header row">
                                        <div class="section_title col-xl-auto col-12">
                                            <h2>محصولات جدید</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="Computer" role="tabpanel">
                                    <div class="product_carousel product_style product_column4 owl-carousel">
                                        <template v-for="(newProduct,index) in newProducts">
                                            <div class="product_items">
                                                <template v-for="(newProduct2,index) in newProduct">
                                                    <article class="single_product">
                                                        <figure>
                                                            <div class="product_thumb">
                                                                <a class="primary_img"
                                                                   :href="'/detail/'+newProduct2['slug']">
                                                                    <img
                                                                        :src="'/images/product/'+newProduct2['image']">
                                                                </a>
                                                                <a class="secondary_img"
                                                                   :href="'/detail/'+newProduct2['slug']">
                                                                    <img
                                                                        :src="'/images/product/'+newProduct2['image']">
                                                                </a>

                                                            </div>
                                                            <div class="product_content">
                                                                <div class="product_content_inner">
                                                                    <h4 class="product_name">
                                                                        <a :href="'/detail/'+newProduct2['slug']">@{{
                                                                            newProduct2['name'] }}</a>
                                                                    </h4>
                                                                    <div class="price_box">
                                                                        <span v-if="checkHaveDiscount(newProduct2['discount'])" class="old_price">@{{ numberFormat(newProduct2['price']) }} تومان</span>
                                                                        <span class="current_price">@{{ calculateDiscount(newProduct2['price'],newProduct2['discount'])}} تومان</span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </figure>
                                                    </article>
                                                </template>
                                            </div>
                                        </template>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--product area start-->
                        <div class="product_area" v-if="offers.length">
                            <div class="row">
                                <div class="col-12">
                                    <div class="product_header row">
                                        <div class="section_title col-xl-auto col-12">
                                            <h2>پیشنهاد به شما</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="Computer3" role="tabpanel">
                                    <div class="product_carousel product_style product_column4 owl-carousel">
                                        <template v-for="(offer,index) in offers">
                                            <article class="single_product">
                                                <figure>
                                                    <div class="product_thumb">
                                                        <a class="primary_img" :href="'/detail/'+offer.slug">
                                                            <img :src="'/images/product/'+offer.image">
                                                        </a>
                                                        <a class="secondary_img" :href="'/detail/'+offer.slug">
                                                            <img :src="'/images/product/'+offer.image">
                                                        </a>
                                                    </div>
                                                    <div class="product_content">
                                                        <div class="product_content_inner">
                                                            <h4 class="product_name">
                                                                <a :href="'/detail/'+offer.slug">@{{ offer.name }}</a>
                                                            </h4>
                                                            <div class="price_box">
                                                                <span v-if="checkHaveDiscount(offer['discount'])" class="old_price">@{{ numberFormat(offer.price) }} تومان</span>
                                                                <span class="current_price">@{{ calculateDiscount(offer.price,offer.discount)}} تومان</span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </figure>
                                            </article>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                cats: [],
                brands: [],
                brandsChosen: [],
                filters: [],
                offers: [],
                loader: false,
                search: '',
                flagMore: true,
                slider: [],
                // online
                newProducts: {},
            },
            computed: {
                filteredList() {
                    return this.brands.filter(brand => {
                        return brand.name.includes(this.search) || brand.name_f.includes(this.search);
                    });
                }
            },
            methods: {
                // startOnline
                fetchproducts() {
                    this.loader = true;
                    let _this = this;
                    axios.get('/fetch/new/products').then(res => {
                        _this.newProducts = res.data;
                    });
                },
                // endOnline
                fetchProducts(page = 1) {
                    this.loader = true;
                    let data = this;
                    axios.get('/fetch/products?page=' + page).then(res => {
                        data.products = res.data;
                    });
                    setTimeout(this.ggg, 2000);
                },
                calculateDiscount(price, discount) {
                    onePercent = price / 100;
                    difference = 100 - discount;
                    total = difference * onePercent;
                    result = Math.round(total);
                    return result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                numberFormat(price) {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                fetchCats() {
                    let data = this;
                    axios.get('/fetch/search/cats').then(function (res) {
                        data.cats = res.data;

                    });
                },
                redirectFilter(cat) {
                    window.location.href = `/search/${cat}`;
                },
                fetchBrand() {
                    let data = this;
                    axios.get(`/fetch/all/brands`).then(res => {
                        data.brands = res.data;
                    });
                },
                replaceUnder(id) {
                    str = id.replace(/\s+/g, '_');
                    return str;
                },
                changeBrand(name) {
                    str = name.replace(/\s+/g, '_');

                    if ($(`#${str}`).prop('checked')) {
                        this.brandsChosen.push(str);

                    } else {
                        index = this.brandsChosen.indexOf(str);
                        this.brandsChosen.splice(index, 1);
                    }
                    this.searchProducts();
                },
                searchProducts() {
                    let data = this;
                    axios.post('/search/brands', {
                        brand: this.brandsChosen,
                    }).then(res => {
                        data.products = res.data;
                    });
                },
                detail(slug) {
                    window.location.href = `/detail/${slug}`;
                },
                fav(id) {
                    let data = this;
                    axios.get(`/add/fav/${id}`).then(res => {
                        swal.fire(
                            {
                                text: "این کالا به لیست علاقه مندی ها افزوده شد !",
                                type: "success",
                                confirmButtonText: 'باشه',
                            }
                        );
                    });
                },
                fetchOffers() {
                    let _this = this;
                    axios.get(`/fetch/offers`).then(res => {
                        _this.offers = res.data;
                    });
                },
                goUp() {
                    $("html, body").animate({scrollTop: 0}, "slow");
                },
                ggg() {
                    this.loader = false;
                },
                more() {
                    this.flagMore = false;
                    $("#categories").addClass("more");
                },
                less() {
                    this.flagMore = true;
                    $("#categories").removeClass("more");
                },
                fetchSlider() {
                    let _this = this;
                    axios.get(`/fetch/slider`).then(res => {
                        _this.slider = res.data;
                    });
                },
                ifActive(index) {
                    if (index === 0) {
                        return 'active'
                    }
                },
                checkHaveDiscount(discount) {
                    if (discount == 0) {
                        return false;
                    } else {
                        return true;
                    }
                },
            },
            mounted: function () {
                this.fetchproducts();
                this.fetchOffers();
                this.fetchSlider();
            }
        })
    </script>

@endsection

@section('style')
    <style>
        body {
            overflow-x: hidden;
        }
    </style>
@endsection

