@extends('layouts.site.app')
@section('content')
    <div id="content" class="mb-5 pb-5">
        <div
            class="levels d-flex flex-row-reverse justify-content-between align-items-center col-10 col-lg-6  mx-auto mt-5">
            <div
                class="cart-lev d-flex flex-column-reverse flex-lg-row justify-content-center align-items-lg-center col-3 current-lev col-lg-auto px-0">
                <span class="mr-lg-2 mt-2 mt-lg-0">سبد خرید</span>
                <img src="/assets/images/shipping-lev-1.svg" alt="" class="px-4 px-lg-1">
            </div>
            <div class="line-lev"></div>
            <div
                class="info-lev d-flex flex-column-reverse flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto px-0">
                <span class="mr-lg-2 mt-2 mt-lg-0">اطلاعات ارسال</span>
                <img src="/assets/images/shipping-lev-2.svg" alt="" class="px-4 px-lg-1">
            </div>
            <div class="line-lev"></div>
            <div
                class="pay-lev d-flex flex-column-reverse flex-lg-row justify-content-center align-items-lg-center col-3 col-lg-auto px-0">
                <span class="mr-lg-2 mt-2 mt-lg-0">اطلاعات پرداخت </span>
                <img src="/assets/images/shipping-lev-3.svg" alt="" class="px-4 px-lg-1">
            </div>
        </div>

        <div v-if="carts.length" class="product-cart-area hm-3-padding pb-130 mt-5 text-right">

            <div class="d-flex flex-row-reverse flex-wrap justify-content-around">
                <div class="cart-desktop col-8 d-none d-lg-block px-0 px-md-2">
                    <div class="rtl">
                        <div class="col-12">
                            <template v-for="cart in carts">
                                <div
                                    class="d-flex flex-wrap justify-content-between py-3 align-items-center cart-items-table">
                                    <div class="product-cart-icon col-1 pr-0  product-subtotal  text-center">
                                        <a @click="deleteCart(cart.id)">
                                            <i class="text-danger" aria-hidden="true"
                                               style="font-size:20px ;font-style: normal">&#10006</i>
                                        </a>
                                    </div>
                                    <div class="product-thumbnail col-lg-2 d-flex align-items-center">
                                        <a :href=`/detail/${cart.product.slug}`>
                                            <img style="width: 100% ;border-radius:7px"
                                                 :src="'/images/product/'+cart.product.image">
                                        </a>
                                    </div>
                                    <div class="product-name col-6 col-lg-3">
                                        <a :href=`/detail/${cart.product.slug}` style="color:#333">
                                            @{{ cart.product.name }}
                                        </a>
                                        <div v-if="cart.size" class="d-block mt-3" style="color:#777">
                                            سایز : @{{ cart.size.name }}
                                        </div>
                                        <div v-if="cart.color" class="d-block mt-2" style="color:#777">
                                            رنگ : @{{ cart.color.name}}
                                        </div>
                                    </div>
                                    <div class="product-price col-lg-3">
                                        <div class="new-price rtl">
                                            @{{ calculateDiscount(cart.product.price,cart.product.discount) }}
                                            تومان
                                        </div>
                                        <div class="old-price rtl mt-2"
                                             v-if="checkHaveDiscount(cart.product.discount)">
                                            @{{ numberFormat(cart.product.price) }}
                                            <span>تومان</span>
                                        </div>
                                    </div>
                                    <div class="product-quantity col-lg-1 d-flex align-items-center">
                                        <select
                                            style="cursor:pointer;border-radius: 5px;padding:0 5px;border-color: #dddddd;color: grey;direction: ltr"
                                            v-model="cart.number"
                                            @change="onChange($event,cart.id)">
                                            <option value="1">1</option>
                                            <option value="2" v-if="checkNum2(cart.exist_num)">2</option>
                                            <option value="3" v-if="checkNum3(cart.exist_num)">3</option>
                                            <option value="4" v-if="checkNum4(cart.exist_num)">4</option>
                                            <option value="5" v-if="checkNum5(cart.exist_num)">5</option>
                                            <option value="6" v-if="checkNum6(cart.exist_num)">6</option>
                                            <option value="7" v-if="checkNum7(cart.exist_num)">7</option>
                                            <option value="8" v-if="checkNum8(cart.exist_num)">8</option>
                                            <option value="9" v-if="checkNum9(cart.exist_num)">9</option>
                                            <option value="10" v-if="checkNum10(cart.exist_num)">10</option>
                                        </select>
                                    </div>
                                    <div class="product-subtotal col-lg-2 text-center"
                                         style="color:#00bf6f;font-size: 16px">
                                        @{{ eachTotal(cart) }} تومان
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
                <div class="cart-mobile col-12 d-lg-none px-0 px-md-2">
                    <div class=" rtl ">
                        <div class="col-12 px-0">
                            <div>
                                <template v-for="cart in carts">
                                    <div
                                        class="d-flex flex-wrap py-3 px-2 position-relative text-right px-0 cart-items-table">
                                        <div
                                            class="product-cart-icon py-1 px-2 product-subtotal  text-center">
                                            <a @click="deleteCart(cart.id)" style="font-size:20px">
                                                <i class="text-danger" aria-hidden="true"
                                                   style="font-style:normal;">&#10006</i>
                                            </a>
                                        </div>
                                        <div class="col-4 px-0">
                                            <div class="product-thumbnail col-12 px-0  d-flex align-items-center">
                                                <a :href=`/detail/${cart.product.slug}`>
                                                    <img style="width: 100%"
                                                         :src="'/images/product/'+cart.product.image">
                                                </a>
                                            </div>
                                        </div>
                                        <div
                                            class="col-7 d-flex justify-content-between align-items-center flex-wrap px-2">
                                            <div class="product-name  col-12 px-0">
                                                <a :href=`/detail/${cart.product.slug}` style="color:#333"> @{{
                                                    cart.product.name }}</a>
                                                <div v-if="cart.size" class="my-2" style="color:#888">
                                                    سایز : @{{ cart.size.name }}
                                                </div>
                                                <div v-if="cart.color" style="color:#888">
                                                    رنگ : @{{ cart.color.name}}
                                                </div>
                                            </div>
                                            <div class="product-quantity d-flex align-items-center mt-3">
                                                <select
                                                    style="cursor:pointer;border-radius: 5px;padding:0 5px;border-color: #dddddd;color: grey;direction: ltr"
                                                    v-model="cart.number"
                                                    @change="onChange($event,cart.id)">
                                                    <option value="1">1</option>
                                                    <option value="2" v-if="checkNum2(cart.exist_num)">2</option>
                                                    <option value="3" v-if="checkNum3(cart.exist_num)">3</option>
                                                    <option value="4" v-if="checkNum4(cart.exist_num)">4</option>
                                                    <option value="5" v-if="checkNum5(cart.exist_num)">5</option>
                                                    <option value="6" v-if="checkNum6(cart.exist_num)">6</option>
                                                    <option value="7" v-if="checkNum7(cart.exist_num)">7</option>
                                                    <option value="8" v-if="checkNum8(cart.exist_num)">8</option>
                                                    <option value="9" v-if="checkNum9(cart.exist_num)">9</option>
                                                    <option value="10" v-if="checkNum10(cart.exist_num)">10</option>
                                                </select>
                                            </div>
                                            <div class="product-subtotal text-center mt-3"
                                                 style="color: #1ac977;font-weight: bold;">
                                                @{{ eachTotal(cart) }} تومان
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-if="carts.length" class="col-lg-3 row mt-5 mt-lg-0 text-right rtl"
                     style="height: fit-content;position:sticky !important;top:15px;">
                    <div class="shop-total col-12 pb-4 pt-3 px-4" style="border: 1px solid #e0e0e0">
                        <div class=" title">
                            <ul class="mt-3 px-0">
                                <li class="order-total d-flex flex-row justify-content-between  align-items-center pb-3">
                                    <span class="rtl">مجموع کل سفارش :</span>
                                    <span class="price pr-4 rtl"> @{{ numberFormat(totalWithoutDiscount) }} تومان</span>
                                </li>
                                <li class="order-total d-flex flex-row justify-content-between align-items-center pb-3">
                                    <span class="rtl">مجموع تخفیف ها :</span>
                                    <span
                                        class="price pr-4 rtl">@{{ numberFormat(Math.round(totalWithoutDiscount) - Math.round(finalTotal)) }} تومان</span>
                                </li>

                                <li class="order-total d-flex flex-row justify-content-between align-items-center pb-3">
                                    <span class="rtl" style="color: #2f2f2f;font-weight: bold;font-size: 15px">مبلغ نهایی سبد خرید :</span>
                                    <span class="price pr-4 rtl"
                                          style="color: #2f2f2f;font-weight: bold;font-size: 15px">@{{ numberFormat(Math.round(finalTotal)) }} تومان</span>
                                </li>

                            </ul>
                        </div>
                        <div class="cart-btn-container text-center ">
                            <a href="/shipping"
                               class="d-block mb-3 col-12 mb-lg-0 buy-btn" style="cursor:pointer">
                                ثبت سفارش و پرداخت

                            </a>
                            <a class="d-block col-12 add-btn mt-2" style="border: 1px solid #e0e0e0"
                               href="/">
                                افزودن محصول جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container mt-5 pt-5">
            <div class="row" v-if="!carts.length">
                <div class="col-12 text-center">
                    <img src="/assets/images/cart-empty.svg" alt="">
                    <p class="text-center mt-3">
                        سبد خرید شما خالی است
                    </p>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                carts: [],
                totalWithoutDiscount: 0,
                finalTotal: 0
            },
            methods: {
                fetchCart() {
                    let vm = this
                    axios.get('/fetch/cart').then(res => {
                        vm.carts = res.data
                        vm.calTotalWithoutDiscount()
                        vm.calTotal()
                    })
                },
                calTotalWithoutDiscount() {
                    this.carts.forEach(cart => {
                        this.totalWithoutDiscount += cart.product.price * cart.number
                    })
                },
                calTotal() {
                    this.carts.forEach(cart => {
                        let per = cart.product.price / 100
                        let x = 100 - cart.product.discount
                        let y = per * x
                        this.finalTotal += y * cart.number
                    })
                },
                calculateDiscount(price, discount) {
                    let onePercent = price / 100
                    let difference = 100 - discount
                    let total = Math.round(difference * onePercent)
                    return total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                },
                async onChange(event, cartId) {
                    let vm = this
                    await axios.post('/cart/total', {
                        cart_id: cartId,
                        number: event.target.value,
                    }).then(() => {
                        vm.totalWithoutDiscount = 0
                        vm.finalTotal = 0
                    })
                    vm.fetchCart()
                },
                deleteCart(id) {
                    let vm = this
                    axios.get(`/cart/delete/${id}`).then(() => {
                        vm.fetchCart()
                        vm.fetchSumTotal()
                        vm.fetchSumPrice()
                    })
                },
                numberFormat(price) {
                    price = Math.trunc(price);
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                checkNum2(num) {
                    if (num >= 2) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum3(num) {
                    if (num >= 3) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum4(num) {
                    if (num >= 4) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum5(num) {
                    if (num >= 5) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum6(num) {
                    if (num >= 6) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum7(num) {
                    if (num >= 7) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum8(num) {
                    if (num >= 8) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum9(num) {
                    if (num >= 9) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkNum10(num) {
                    if (num >= 10) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkHaveDiscount(discount) {
                    return (discount == 0) ? false : true
                },
                eachTotal(cart) {
                    let onePercent = cart.product.price / 100
                    let difference = 100 - cart.product.discount
                    let total = Math.round(difference * onePercent) * cart.number

                    return total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                },
            },
            mounted() {
                this.fetchCart()
            }
        })
    </script>


@endsection

@section('style')
    <style>
        input, select {
            background: white
        }

        body {
            background: white;
        }

        .old-price {
            text-decoration: line-through;
            color: #a5a5a5;
        }

        .new-price {
            color: #3e3e3e;
            font-size: 16px;
        }
    </style>
@endsection

