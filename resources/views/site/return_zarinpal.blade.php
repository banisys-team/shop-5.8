<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>فروشگاه پوشاک</title>
    <script src="{{ asset('/js/dress/app.js')}}"></script>
    <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout_kaj/css/style.css">
    <link rel="stylesheet" href="/layout_kaj/css/swiper.css">
    <style>
        .btn-primary {
            font-size: 14px;
            border-bottom: 3px solid #005abb;
        }
    </style>
</head>
<body>


<div class="container rtl text-right mt-5">
    <div class="row">
        <div class="col-12">
            <?php
            $MerchantID = '7e0c3e5e-77d3-421d-ae62-f8e64310c080';
            $holder = \App\Models\Holder::where('authority', $_GET['Authority'])->first();
            if ($_GET['Status'] == 'OK') {
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $result = $client->PaymentVerification([
                'MerchantID' => $MerchantID,
                'Authority' => $_GET['Authority'],
                'Amount' => $holder->final_total + $holder->delivery + $holder->vat,
            ]);
            //            if ($result->Status == 100) {
            if (100 == 100) {
            //            (new App\Http\Controllers\Site\PanelController())->storeOrder($_GET['Authority'], $result->RefID);
            (new App\Http\Controllers\Site\PanelController())->storeOrder($_GET['Authority'], '132632200');
            ?>

            <div class="alert alert-success" role="alert">
                <p>پرداخت شما با موفقیت انجام شد.</p>
                {{--                <p>شناسه پرداخت شما: <span> {{$result->RefID}} </span></p>--}}
                <p> شناسه پرداخت شما: <span style="font-weight: bold"> 123456 </span></p>
            </div>
            <div style="float: left;">
                <a href="{{url('/panel/orders')}}" style="font-size: 13px;border-bottom: 3px solid #c50000;"
                   class="btn btn-danger ml-4">ادامه ثبت سفارش</a>
            </div>

            <?php
            }
            } else {
            ?>
            <div class="alert alert-danger" role="alert">
                پرداخت انجام نشد.
            </div>
            <div class="buttons" style="float: left">
                <div class="pull-right">
                    <a href="{{url('/')}}" class="btn btn-primary">بازگشت به صفحه اصلی</a>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
</body>

</html>
