@extends('layouts.admin.app')
@section('content')
    <div class="container mt-5 pb-5" id="content" style="position: relative">
        <div class="row mt-4">
            <div class="col-12">
                <div class="scrollme">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">نام</th>
                            <th scope="col">موبایل</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(contact,index) in contacts.data">
                            <td>@{{contact.name}}</td>
                            <td>@{{contact.mobile}}</td>
                            <td>
                                <a @click="showDescription(contact.id)" class="btn btn-primary"
                                   style="color: white;font-size: 13px">
                                    متن پیام
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row mt-2 mr-3">
            <div class="col-12" style="text-align:center">
                <pagination :limit="1" :data="contacts" @pagination-change-page="fetch"></pagination>
            </div>
        </div>


        <div v-show="modalCart">
            <div id="modal-back" @click="flagModalCart = false"></div>
            <div class="modal-content col-12 col-lg-6">
                <div class="row">
                    <div class="col-12 text-center relative p-4">
                        <p>@{{ ticket }}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                contacts: [],
                showModal: false,
                ticket: '',
                flagModalCart: false,
            },
            computed: {
                modalCart() {
                    return this.flagModalCart
                }
            },
            methods: {
                fetch() {
                    let _this = this;
                    axios.get(`/admin/complaint/fetch`).then(res => {
                        _this.contacts = res.data
                    })
                },
                showDescription(id) {
                    let _this = this;
                    this.flagModalCart = true

                    axios.get(`/admin/complaint/ticket/${id}`).then(function (res) {
                        _this.ticket = res.data
                    });
                },
            },
            mounted() {
                this.fetch()
            }
        })
    </script>

    <script>
        $("#side_complaint").addClass("menu-open");
        $("#side_complaint_create").addClass("active");
    </script>
@endsection
@section('style')
    <style>
        #modal-back {
            z-index: 99998;
            left: 0;
            width: 100%;
            height: 100vh;
            background-color: #00000096;
            position: fixed;
            top: 0;
        }

        .modal-mask {
            position: fixed !important;
            top: 0 !important;
            left: 0 !important;
            width: 100% !important;
            height: 100% !important;
            background-color: #00000073 !important;
            display: table !important;
            transition: opacity .3s ease !important;
        }

        .modal-content p {
            font-size: 14px;
            text-align: right
        }

        .modal-content input {
            border: 1px solid #ddd;
            padding: 5px;
            border-radius: 37px;
        }

        .modal-content {
            position: fixed;
            top: 13%;
            left: 50%;
            transform: translateX(-50%);
            border-radius: 20px;
            z-index: 99999;
        }

        .modal-content .form-control:focus {
            color: unset;
            background-color: unset;
            border-color: unset;
            outline: unset;
            box-shadow: unset;
        }

        .scrollme {
            overflow-x: auto;
        }
    </style>
@endsection
