@extends('layouts.admin.app')
@section('content')
    <div class="container mt-4" id="content">
        <div class="row">
            <div class="col-lg-3">
                <div class="row text-center month">
                    <div class="col-4 my-2">
                        <span>فروردین</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>اردیبهشت</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>خرداد</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>تیر</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>مرداد</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>شهریور</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>مهر</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>آبان</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>آذر</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>دی</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>بهمن</span>
                    </div>
                    <div class="col-4 my-2">
                        <span>اسفند</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {

            },
            methods: {

            },
            mounted() {

            }
        })
    </script>

@endsection

@section('style')
<style>
    .month span{
        width: 80px;
        color: #808080;
        padding: 4px 0 6px 0;
        border: 1px solid #b7b7b7;
        border-radius: 20px;
        display: inline-block;
    }
    #side_bani{
        background: whitesmoke;
        border-radius: 0px 8px 8px 0;
    }
    #side_bani a{
        color: #343a40;
        font-weight: bold;
    }
</style>
@endsection
